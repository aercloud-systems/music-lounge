[![Crates.io](https://img.shields.io/crates/v/music-lounge)](https://crates.io/crates/music-lounge)
[![AUR version](https://img.shields.io/aur/version/music-lounge)](https://aur.archlinux.org/packages/music-lounge/)
# Music Lounge
![](icon.png)

Music Lounge is yet another music player app for Unix-like systems. It will be the default music player for Aercloud OS. Supported music encodings are MP3 (.mp3), WAV (.wav), Vorbis (.ogg), and FLAC (.flac).

# Screenshots

## Main UI (Classic Layout)
![](screenshots/library.png)

## Song Art View
![](screenshots/album_art.png)

## Playlist Editor
![](screenshots/playlist_editor.png)

## Metropolis Layout
![](screenshots/metropolis.png)

## Cupertino Layout
![](screenshots/cupertino.png)

## Dynamic Player Theme Based on Song Art
![](screenshots/colors.png)

## Text Based and Graphical Audio Visualizers
![](screenshots/matrix-vis.png)

![](screenshots/wave-vis.png)

![](screenshots/text-matrix-vis.png)

![](screenshots/text-wave-vis.png)

# How to install

## Arch Linux based Linux systems

Music Lounge is available in the AUR for Arch Linux and any system based on it (like Manjaro Linux, EndeavourOS, and Artix Linux)

Installation example using `yay`: `yay -S music-lounge`

## Other Linux and Unix-like systems

Make sure you have the latest version of Rust installed

Instructions on how to install it are [here](https://rustup.rs/)

There is also a good possibility that Rust may be available in your distro's package repositories. Install it with your package manager if this is true.

After installing Rust run the following commands:

`git clone https://gitlab.com/NoahJelen/music-lounge`

`cd music-lounge`

`./build.sh `  <-- This will request root access in order to install the program

To remove: run `./remove.sh`

To do:
- [ ] Smart playlists
- [ ] Use `bliss-audio` crate for the ability to create Spotify style playlists
- [ ] Lyrics lookup for songs using Genius
- [ ] Allow connecting to player process from remote machines
- [ ] GUI Support
- [ ] Android support
- [ ] Kodi add on UI
- [ ] Music library editor
- [x] Unique artwork for album singles
- [x] Split into multiple crates
