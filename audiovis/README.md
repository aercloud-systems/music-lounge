[![Crates.io](https://img.shields.io/crates/v/audiovis)](https://crates.io/crates/audiovis)
# Audio Visualization library

This crate is an audio spectrum visualization library that visualizes audio frequencies as
floats from 0 to 1. This crate is also used by Music Lounge.

## Features
* `piston`: piston graphics engine support