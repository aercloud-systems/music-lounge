#![doc = include_str!("../README.md")]

use std::{
    cmp::max,
    cell::RefCell,
    sync::{Arc, Mutex}
};
use cpal::{
    Stream,
    traits::{DeviceTrait, HostTrait, StreamTrait}
};
use ringbuffer::{AllocRingBuffer, RingBuffer};
use spectrum_analyzer::{
    FrequencyLimit,
    FrequencyValue,
    samples_fft_to_spectrum,
    scaling::divide_by_N,
    windows::hann_window
};

#[cfg(feature = "piston")]
use piston_window::{Event, PistonWindow};

/// An audio visualizer that can be drawn in a Piston window
#[cfg(feature = "piston")]
pub trait AudioVisualizer {
    /// Draw the visualizer
    fn draw(&mut self, event: &Event, window: &mut PistonWindow);

    /// Refresh the visualizer
    fn refresh(&mut self);

    /// Set the size of the visualizer
    fn set_size(&mut self, _width: usize, _height: usize) { }
}

type AudioBuffer = Arc<Mutex<AllocRingBuffer<f32>>>;

/// An audio spectrum visualizer that visualizes audio frequencies
/// as `f32`s from 0 to 1
pub struct RawVisualizer {
    raw_stream: Stream,
    buffer: AudioBuffer,
    wave_data: RefCell<Vec<f32>>,
    sample_rate: usize
}

impl RawVisualizer {
    /// Create a new `RawVisualizer
    pub fn new() -> RawVisualizer {
        let device = cpal::default_host().default_input_device().expect("I thought this is playing audio!");
        let sample_rate = device.default_input_config().expect("Where is the input config?").sample_rate().0 as usize;
        let mut raw_buf = AllocRingBuffer::new((5 * sample_rate).next_power_of_two());
        raw_buf.fill(0.);
        let buffer = Arc::new(Mutex::new(raw_buf));
        let buf_ref = buffer.clone();
        let dev_cfg = device.default_input_config().unwrap().config();
        let is_mono = dev_cfg.channels == 1;
        let raw_stream = device
            .build_input_stream(&dev_cfg,
                move |data: &[f32], _info| {
                    let mut audio_buf = buf_ref.lock().unwrap();
                    // Audio buffer only contains Mono data
                    if is_mono {
                        audio_buf.extend(data.iter().copied());
                    }
                    else {
                        // interleaving for stereo is LRLR (de-facto standard?)
                        audio_buf.extend(data.chunks_exact(2).map(|vals| (vals[0] + vals[1]) / 2.))
                    }
                },
                |_err| { },
                None
            ).unwrap();
        let wave_data = RefCell::new(vec![0.; 1024]);

        RawVisualizer {
            raw_stream,
            buffer,
            wave_data,
            sample_rate
        }
    }

    /// Get the data from the currently playing audio
    pub fn get_wave_data(&self, num_bars: usize) -> Vec<f32> {
        let audio = self.buffer.lock().unwrap().to_vec();
        let mut wave_data = self.wave_data.borrow().to_vec();

        // spectrum analysis only of the latest 46ms
        let skip_elements = audio.len() - 2048;
        let relevant_samples = &audio[skip_elements..skip_elements + 2048];

        // do FFT
        let hann_window = hann_window(relevant_samples);
        let latest_wave = samples_fft_to_spectrum(
            &hann_window,
            self.sample_rate as u32,
            FrequencyLimit::All,
            Some(&divide_by_N),
        )
            .unwrap();

        // now smoothen the spectrum; old values are decreased a bit and replaced,
        // if the new value is higher
        let data = latest_wave.data();
        for ((_, fr_val_new), fr_val_old) in data.iter().zip(wave_data.iter_mut()) {
            let old_val = *fr_val_old * 0.5;
            let max = max(
                *fr_val_new * 1000f32.into(),
                FrequencyValue::from(old_val),
            );
            *fr_val_old = max.val();
        }

        *(self.wave_data.borrow_mut()) = wave_data;
        let mut processed_wave = vec![];
        for (bar_num, f) in self.wave_data.borrow().iter().enumerate() {
            let val = f / 25.;
            if val > 1. { processed_wave.push(1.); }
            else { processed_wave.push(val); }
            if bar_num >= num_bars { break; }
        }
        processed_wave
    }
}

impl Default for RawVisualizer {
    fn default() -> Self { Self::new() }
}

impl Drop for RawVisualizer {
    fn drop(&mut self) { self.raw_stream.pause().unwrap(); }
}