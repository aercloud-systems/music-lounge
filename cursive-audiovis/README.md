[![Crates.io](https://img.shields.io/crates/v/cursive-audiovis)](https://crates.io/crates/cursive-audiovis)
# Cursive Audio Visualizers

Audio visualization views for the cursive TUI library
