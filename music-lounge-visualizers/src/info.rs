use image::{
    io::Reader as ImageReader,
    imageops::FilterType
};
use colorgrad::{Color as GradientColor, CustomGradient, Gradient};
use piston_window::{
    types::Color as PistonColor,
    clear,
    image as draw_image,
    Event,
    Glyphs,
    Texture,
    Rectangle,
    DrawState,
    G2dTexture,
    Transformed,
    PistonWindow,
    TextureSettings,
    G2dTextureContext,
    text,
    line
};
use rand::{
    Rng, thread_rng,
    seq::{SliceRandom, IteratorRandom}, rngs::ThreadRng
};
use audiovis::{AudioVisualizer, RawVisualizer};
use mlounge_client::MLoungeClient;
use crate::CONFIG;

pub struct GLInfoWidget {
    rand: ThreadRng,
    wave: RawVisualizer,
    player: MLoungeClient,
    glyph_cache: Glyphs,
    img_context: G2dTextureContext,
    latest_data: Vec<f32>,
    art_path: String,
    colors: Vec<PistonColor>,
    vis_border: PistonColor,
    vis_gradient: Gradient,
    back_color: PistonColor,
    prog_color: PistonColor,
    prog_border: PistonColor,
    startup: bool
}

impl GLInfoWidget {
    pub fn new(glyph_cache: Glyphs, img_context: G2dTextureContext) -> GLInfoWidget {
        GLInfoWidget {
            rand: thread_rng(),
            wave: RawVisualizer::new(),
            player: MLoungeClient::new(),
            glyph_cache,
            img_context,
            latest_data: vec![0.; 4],
            art_path: String::new(),
            colors: vec![[90. / 256., 174. / 256., 222. / 256., 1.]],
            vis_border: [90. / 256., 174. / 256., 222. / 256., 1.],
            vis_gradient: colorgrad::turbo(),
            back_color: [0.5; 4],
            prog_color: [1.; 4],
            prog_border: [1.; 4],
            startup: true
        }
    }

    fn gen_gradient(&self) -> Gradient {
        let mut rand = thread_rng();
        let num_colors = rand.gen_range(2..20);
        let colors: Vec<GradientColor> = self.colors.choose_multiple(&mut rand, num_colors)
            .map(|[r, g, b, _]| GradientColor::new(*r as f64, *g as f64, *b as f64, 1.))
            .collect();

            CustomGradient::new().colors(&colors).build().unwrap_or_else(|_| colorgrad::turbo())
    }

    fn get_album_image(&mut self) -> G2dTexture {
        let path = if !self.art_path.is_empty() { &self.art_path } else { &CONFIG.conky_placeholder };
        let img = ImageReader::open(path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(125, 125, FilterType::Lanczos3);

        Texture::from_image(
            &mut self.img_context,
            &img.to_rgba8(),
            &TextureSettings::new()
        ).unwrap()
    }
}

impl AudioVisualizer for GLInfoWidget {
    fn draw(&mut self, event: &Event, window: &mut PistonWindow) {
        window.draw_2d(event, |ctx, gfx, dev| {
            self.back_color[3] = 0.5;
            clear(self.back_color, gfx);
            draw_image(&self.get_album_image(), ctx.transform.trans(5., 5.), gfx);
            text([1.; 4], 10, &self.player.song.name, &mut self.glyph_cache, ctx.transform.trans(135., 16.), gfx).expect("Unable to render text!");
            text([1.; 4], 10, &self.player.song.album, &mut self.glyph_cache, ctx.transform.trans(135., 32.), gfx).expect("Unable to render text!");
            text([1.; 4], 10, &self.player.song.artist, &mut self.glyph_cache, ctx.transform.trans(135., 48.), gfx).expect("Unable to render text!");
            let duration = self.player.song.get_duration_str();
            let cur_time = self.player.cur_time();
            text([1.; 4], 10, &format!("{cur_time}/{duration}"), &mut self.glyph_cache, ctx.transform.trans(135., 64.), gfx).expect("Unable to render text!");
            let prog_border = Rectangle::new_border(self.prog_border, 1.);
            let prog_bar = Rectangle::new(self.prog_color);
            let duration = self.player.song.duration;
            let cur_time = self.player.cur_time_secs();
            prog_bar.draw([135., 70., (cur_time as f64 / duration as f64) * 230., 15.], &DrawState::default(), ctx.transform, gfx);
            prog_border.draw([135., 70., 230., 14.], &DrawState::default(), ctx.transform, gfx);
            for i in 0..230 {
                let color = self.vis_gradient.at(i as f64 / 230.);
                let p_color = [color.r as f32, color.g as f32, color.b as f32, 1.];
                line(p_color, 0.5, [135. + i as f64, 90., 135. + i as f64, 104.], ctx.transform, gfx);
                if i as f64 > (self.latest_data[3] as f64 * 230.) - 1. { break; }
            }
            let vis_border = Rectangle::new_border(self.vis_border, 1.);
            vis_border.draw([135., 90., 230., 14.], &DrawState::default(), ctx.transform, gfx);
            self.glyph_cache.factory.encoder.flush(dev);
        });
    }

    fn refresh(&mut self) {
        self.latest_data = self.wave.get_wave_data(4);
        if self.player.song_changed() || self.startup {
            self.player.sync(true).unwrap();
            self.colors = self.player.song.piston_colors();
            if let Some(img) = self.player.song.get_art_path() {
                self.back_color = get_color(&self.colors, true);
                self.art_path = img;
                self.prog_color = get_color(&self.colors, false);
                self.prog_border = get_color(&self.colors, false);
                self.vis_gradient = self.gen_gradient();
                self.vis_border = *(self.colors.choose(&mut self.rand).unwrap());
            }
            else {
                self.colors = vec![[90. / 256., 174. / 256., 222. / 256., 1.]];
                self.back_color = [0.5; 4];
                self.art_path = String::new();
                self.prog_color = [1.; 4];
                self.prog_border = [1.; 4];
                self.vis_gradient = colorgrad::turbo();
                self.vis_border = [90. / 256., 174. / 256., 222. / 256., 1.];
            }
            self.startup = false;
        }
        if self.latest_data[3] >= 0.25 && !self.colors.is_empty() {
            self.vis_gradient = self.gen_gradient();
            self.vis_border = *(self.colors.choose(&mut self.rand).unwrap());
        }
    }
}

// get a dark or bright color
fn get_color(colors: &[PistonColor], dark: bool) -> PistonColor {
    let mut rand = thread_rng();
    if dark {
        let colors_iter = colors.iter().filter(|c| get_brightness(**c) <= 85);
        if let Some(color) = colors_iter.choose(&mut rand) {
            *color
        }
        else {
            let num: u8 = rand.gen_range(0..85);
            [num as f32 / 255.; 4]
        }
    }
    else {
        let colors_iter = colors.iter().filter(|c| get_brightness(**c) >= 131);
        if let Some(color) = colors_iter.choose(&mut rand) {
            *color
        }
        else {
            let num: u8 = rand.gen_range(131..=255);
            [num as f32 / 255.; 4]
        }
    }
}

// get the brightness of a color
fn get_brightness(color: PistonColor) -> u8 {
    ((299 * (color[0] * 255.).ceil() as u32 + 587 * (color[1] * 255.).ceil() as u32 + 114 * (color[2] * 255.).ceil() as u32) / 1000) as u8
}