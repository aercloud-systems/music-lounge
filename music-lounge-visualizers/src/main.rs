use std::{env, fs, fmt::Write};
use piston_window::{
    AdvancedWindow,
    EventLoop as _,
    PistonWindow,
    RenderEvent,
    ResizeEvent,
    UpdateEvent,
    Window,
    WindowSettings
};
use glutin_window::{GlutinWindow, OpenGL, UserEvent};
use image::io::Reader as ImageReader;
use lazy_static::lazy_static;
use rust_utils::utils;
use winit::{
    dpi::LogicalSize,
    event_loop::EventLoopBuilder,
    window::{
        Icon,
        WindowBuilder
    }
};
use mlounge_core::config::{MLConfig, Config};
use audiovis::AudioVisualizer;

mod info;
mod wave;
mod matrix;

use info::GLInfoWidget;
use matrix::GLMatrixVisualizer;
use wave::GLWaveVisualizer;

//TODO: visualizer configs
//TODO: make the text scroll in the info window if it does not fit in the window
//TODO: make the visualizers "chase" the audio output of music lounge
lazy_static! {
    static ref CONFIG: MLConfig = MLConfig::load();
}

fn main() {
    // get the runtime arguments
    let prg_name = utils::get_execname();
    let args: Vec<String> = env::args().collect();
    let name = args.get(1).unwrap_or(&prg_name);
    let short_name = name.replace("mlounge-", "");
    let mut window = gen_win(&short_name);
    match short_name.as_str() {
        "wave" => vis_loop(GLWaveVisualizer::new(), window),

        "matrix" => {
            let font =
                if fs::read("/usr/share/mlounge/font/Monocraft.otf").is_err() { "font/Monocraft.otf" }
                else { "/usr/share/mlounge/font/Monocraft.otf" };

            let glyph_cache = window.load_font(font).unwrap();
            vis_loop(GLMatrixVisualizer::new(glyph_cache), window);
        }

        "info" => {
            window.set_position([90, 65]);
            let font =
                if fs::read("/usr/share/mlounge/font/Monocraft.otf").is_err() { "font/Monocraft.otf" }
                else { "/usr/share/mlounge/font/Monocraft.otf" };

            let glyph_cache = window.load_font(font).unwrap();
            vis_loop(GLInfoWidget::new(glyph_cache, window.create_texture_context()), window);
        }

        _ => {
            eprintln!("Invalid visualizer name!\"{short_name}\"");
            println!("Valid names are matrix, wave, and info");
        }
    }
}

fn vis_loop<V: AudioVisualizer>(mut visualizer: V, mut window: PistonWindow) {
    visualizer.set_size(window.size().width.ceil() as usize, window.size().height.ceil() as usize);
    while let Some(event) = window.next() {
        if event.update_args().is_some() {
            visualizer.refresh();
        }

        if event.render_args().is_some() {
            visualizer.draw(&event, &mut window);
        }

        if let Some(args)  = event.resize_args() {
            let new_size = args.window_size;
            visualizer.set_size(new_size[0].ceil() as usize, new_size[1].ceil() as usize);
        }
    }
}

// create the window for the visualizer
fn gen_win(name: &str) -> PistonWindow {
    let mut title = format!("Music Lounge {}", utils::capitalize(name));
    if name != "info" { write!(title, " Visualizer").unwrap(); }

    // the icon image
    let icon_img = if fs::read("/usr/share/mlounge/icon.png").is_err() { "icon.png" }
    else { "/usr/share/mlounge/icon.png" };
    let img = ImageReader::open(icon_img).unwrap().with_guessed_format().unwrap().decode().unwrap();
    let img_bytes = img.as_bytes();
    let icon = Icon::from_rgba(img_bytes.to_vec(), 256, 256).unwrap();

    // event loop
    let eloop = EventLoopBuilder::<UserEvent>::with_user_event().build();

    let size = if name == "info" { [375, 135] }
    else { [1280, 720] };

    // the raw winit window
    let mut win_raw = WindowBuilder::new()
        .with_window_icon(Some(icon))
        .with_resizable(true)
        .with_transparent(true)
        .with_maximized(true)
        .with_decorations(false)
        .with_title(&title);

    if name == "info" {
        win_raw = win_raw
            .with_inner_size(LogicalSize::new(375, 135))
            .with_resizable(false);
    }

    // the window's settings
    let mut win_cfg = WindowSettings::new(&title, size).transparent(true).resizable(true);
    if name == "info" { win_cfg = win_cfg.resizable(false); }

    // create the window
    let gl_win = GlutinWindow::from_raw(&win_cfg, eloop, win_raw).unwrap();
    PistonWindow::new(OpenGL::V4_5, 0, gl_win)
        .ups_reset(2)
        .max_fps(60)
        .bench_mode(true)
        .capture_cursor(false)
}