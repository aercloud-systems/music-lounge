use gfx_device_gl::Device;
use piston_window::{
    text,
    clear,
    G2d,
    Glyphs,
    Context,
    Transformed,
    Event,
    PistonWindow,
    types::Color as PistonColor
};
use rand::{
    Rng, thread_rng,
    rngs::ThreadRng,
    seq::{
        SliceRandom as _,
        IteratorRandom as _
    }
};
use audiovis::{AudioVisualizer, RawVisualizer};
use mlounge_client::MLoungeClient;

pub struct GLMatrixVisualizer {
    rand: ThreadRng,
    wave: RawVisualizer,
    latest_data: Vec<f32>,
    colors: Vec<PistonColor>,
    glyph_cache: Glyphs,
    player: MLoungeClient,
    width: usize,
    height: usize,
    droplets: Vec<Droplet>,
    startup: bool
}

impl GLMatrixVisualizer {
    pub fn new(glyph_cache: Glyphs) -> GLMatrixVisualizer {
        GLMatrixVisualizer {
            rand: thread_rng(),
            wave: RawVisualizer::new(),
            latest_data: vec![0.;4],
            colors: vec![[51. / 197., 174. / 256., 78. / 256., 1.]],
            glyph_cache,
            player: MLoungeClient::new(),
            width: 0,
            height: 0,
            droplets: Vec::new(),
            startup: true
        }
    }

    fn add_droplet(&mut self) {
        if self.width == 0 || self.height == 0 {
            return;
        }
        let x = self.rand.gen_range(0..self.width);
        let length = self.rand.gen_range(20..self.height / 20);
        let color = self.colors.choose(&mut self.rand).unwrap();
        self.droplets.push(Droplet::new(x as f64, length, *color, &mut self.rand));
    }
}

impl AudioVisualizer for GLMatrixVisualizer {
    fn draw(&mut self, event: &Event, window: &mut PistonWindow) {
        window.draw_2d(event, |ctx, gfx, dev| {
            clear([0.; 4], gfx);
            for droplet in &self.droplets {
                droplet.draw(ctx, gfx, &mut self.glyph_cache, dev);
            }
        });
    }

    fn refresh(&mut self) {
        let mut latest_rev = self.wave.get_wave_data(self.width / 20);
        let mut latest = latest_rev.clone();
        latest.reverse();
        latest.append(&mut latest_rev);
        latest.truncate(self.width);
        self.latest_data = latest;
        self.droplets.retain(|droplet| droplet.y - (droplet.chars.len() * (droplet.font_size + 6)) as f64 <= self.height as f64);

        for droplet in &mut self.droplets {
            droplet.fall(&mut self.rand);
        }

        if self.player.song_changed() || self.startup {
            while let Err(_) = self.player.sync(true) { }
            let new_colors = self.player.song.piston_colors();
            if !new_colors.is_empty() {
                self.colors = new_colors;
            }

            self.startup = false;
        }

        for droplet in &mut self.droplets {
            let d_loc = (droplet.x / 10.) as usize;
            if let Some(freq) = self.latest_data.get(d_loc) {
                droplet.flicker = *freq >= 0.25;
            }
        }

        if self.droplets.len() <= self.width / 5 {
            self.add_droplet();
        }
    }

    fn set_size(&mut self, width: usize, height: usize) {
        self.width = width;
        self.height = height;
    }
}

struct Droplet {
    x: f64,
    y: f64,
    chars: String,
    color: PistonColor,
    flicker: bool,
    font_size: usize
}

impl Droplet {
    fn new(x: f64, length: usize, color: PistonColor, rand: &mut ThreadRng) -> Droplet {
        Droplet {
            x,
            y: 0.,
            chars: gen_rand_str(length, rand),
            color,
            flicker: false,
            font_size: rand.gen_range(5..40)
        }
    }

    fn fall(&mut self, rand: &mut ThreadRng) {
        self.flicker = false;
        self.chars.insert(0, get_rand_char(rand));
        self.chars.pop();
        self.y += self.font_size as f64 + 6.;
    }

    fn draw(&self, ctx: Context, gfx: &mut G2d, glyph_cache: &mut Glyphs, dev: &mut Device) {
        let num_chars = self.chars.chars().count();
        for (i, char) in self.chars.chars().enumerate() {
            let new_color =
                if i == 0 || self.flicker { [1., 1., 1., 1.] }
                else {
                    let mut color = self.color;
                    color[3] = (num_chars - i) as f32 / num_chars as f32;
                    color
                };
            let char_offset = (i * (self.font_size + 6)) as f64;
            if char_offset > self.y { return; }
            text(new_color, self.font_size as u32, &char.to_string(), glyph_cache, ctx.transform.trans(self.x, self.y - char_offset), gfx).expect("Unable to render text!");
            glyph_cache.factory.encoder.flush(dev);
        }
    }
}

fn gen_rand_str(length: usize, rand: &mut ThreadRng) -> String {
    std::iter::repeat(())
        .map(|_| get_rand_char(rand))
        .take(length)
        .collect()
}

fn get_rand_char(rand: &mut ThreadRng) -> char {
    let valid_chars = (33..127).chain(161..255);
    char::from_u32(valid_chars.choose(rand).unwrap()).unwrap()
}