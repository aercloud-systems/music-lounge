use colorgrad::{CustomGradient, Color, Gradient};
use piston_window::{
    line, clear,
    G2d,
    Event,
    Context,
    PistonWindow,
    types::Color as PistonColor
};
use rand::{
    Rng,
    thread_rng,
    seq::SliceRandom,
    rngs::ThreadRng
};
use audiovis::{AudioVisualizer, RawVisualizer};
use mlounge_client::MLoungeClient;

pub struct GLWaveVisualizer {
    rand: ThreadRng,
    wave: RawVisualizer,
    latest_data: Vec<f32>,
    colors: Vec<PistonColor>,
    gradient: Gradient,
    cur_color: PistonColor,
    player: MLoungeClient,
    width: usize,
    height: usize,
    startup: bool
}

impl GLWaveVisualizer {
    pub fn new() -> GLWaveVisualizer {
        GLWaveVisualizer {
            rand: thread_rng(),
            wave: RawVisualizer::new(),
            latest_data: vec![0.; 4],
            colors: vec![[90. / 256., 174. / 256., 222. / 256., 1.]],
            gradient: colorgrad::turbo(),
            cur_color: [90. / 256., 174. / 256., 222. / 256., 1.],
            player: MLoungeClient::new(),
            width: 0,
            height: 0,
            startup: true
        }
    }

    fn gen_gradient(&mut self) -> Gradient {
        let num_colors = self.rand.gen_range(5..20);
        let colors: Vec<Color> = self.colors.choose_multiple(&mut self.rand, num_colors).map(|[r, g, b, _]| Color::new(*r as f64, *g as f64, *b as f64, 1.)).collect();
        CustomGradient::new().colors(&colors).build().unwrap_or_else(|_| colorgrad::turbo())
    }

    fn draw_gradient_rect(&self, ctx: Context, gfx: &mut G2d, bar_num: usize, freq: f32, width: f64, max_height: f64) {
        let rect_height = freq as f64 * max_height;
        let line_x = bar_num as f64 * width;
        let line_y = self.height as f64 - rect_height;

        for i in 0..max_height.ceil() as usize {
            let color = self.gradient.at(i as f64 / max_height);
            let p_color = [color.r as f32, color.g as f32, color.b as f32, 1.];
            line(p_color, 0.5, [line_x + 1., self.height as f64 - i as f64, line_x + width - 1., self.height as f64 - i as f64], ctx.transform, gfx);
            if i as f64 > rect_height - 1. { break; }
        }
        line(self.cur_color, 0.5, [line_x + 1., line_y, line_x + width - 1., line_y], ctx.transform, gfx);
        line(self.cur_color, 0.5, [line_x + 1., line_y, line_x + 1., self.height as f64], ctx.transform, gfx);
        line(self.cur_color, 0.5, [line_x + width - 1., line_y, line_x + width - 1., self.height as f64], ctx.transform, gfx);
    }
}

impl AudioVisualizer for GLWaveVisualizer {
    fn draw(&mut self, event: &Event, window: &mut PistonWindow) {
        window.draw_2d(event, |ctx, gfx, _dev| {
            clear([0.;4], gfx);
            // the number of frequency bars to render
            let num_bars = self.width / 10;
            let max_height = self.height as f64 / 4.;
            let rect_width = self.width as f64 / num_bars as f64;
            for (i, freq) in self.latest_data.iter().rev().chain(&self.latest_data).enumerate() {
                self.draw_gradient_rect(ctx, gfx, i, *freq, rect_width, max_height);
            }
        });
    }

    fn refresh(&mut self) {
        let mut num_bars = self.width / 10;
        if num_bars % 2 != 0 { num_bars += 1; } // why is testing for even and odd numbers so hard in JS? /s
        self.latest_data = self.wave.get_wave_data(num_bars / 2);
        if self.player.song_changed() || self.startup {
            while let Err(_) = self.player.sync(true) { }
            let new_colors = self.player.song.piston_colors();
            if !new_colors.is_empty() {
                self.colors = new_colors;
            }
            self.gradient = self.gen_gradient();
            self.cur_color = *(self.colors.choose(&mut self.rand).unwrap());
            self.startup = false;
        }
        if self.latest_data[3] >= 0.25 && !self.colors.is_empty() {
            self.gradient = self.gen_gradient();
            self.cur_color = *(self.colors.choose(&mut self.rand).unwrap());
        }
    }

    fn set_size(&mut self, width: usize, height: usize) {
        self.width = width;
        self.height = height;
    }
}