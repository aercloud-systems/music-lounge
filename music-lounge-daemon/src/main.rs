use std::{
    env, process, fs,
    sync::RwLock,
    net::Shutdown,
    os::unix::net::{UnixStream, UnixListener},
    io::{BufReader, Read, Write}, thread,
};
use crossbeam_channel::bounded;
use lazy_static::lazy_static;
use rust_utils::{
    utils,
    logging::{Log, LogLevel}
};
use sysinfo::System;
use mlounge_core::{
    config::{Config, MLConfig},
    player::{PlayerCommand, PlayerResult},
    library::Library
};

mod daemon;
mod source;
mod player;
mod mpris;

use player::Player;
use daemon::PLAYLISTS;

lazy_static!{
    static ref PLAYER: RwLock<Player> = RwLock::new(Player::new());
    static ref TMP_DIR: String =  format!("/tmp/mlounge-{}", env::var("USER").expect("What is your name again?"));
    static ref LOG: Log = Log::new("mlounged", "music-lounge");
    static ref CONFIG: MLConfig = MLConfig::load();
    static ref SOCKET: String = format!("{}/socket", *TMP_DIR);
    static ref LIBRARY: RwLock<Library> = RwLock::new(build_music_lib(false));
}

#[cfg(debug_assertions)]
lazy_static! {
    static ref LAUNCH_DIR: String = env::current_dir()
        .unwrap()
        .display()
        .to_string();
}

//TODO: make the background process send images to clients
fn main() {
    #[cfg(not(debug_assertions))]
    let vis_prog = "/usr/bin/mlounge-visual";

    #[cfg(debug_assertions)]
    let vis_prog = {
        lazy_static::initialize(&LAUNCH_DIR);
        let vis_path = format!("{}/mlounge-visual", *LAUNCH_DIR);
        if fs::read(&vis_path).is_err() {
            format!("{}/target/debug/mlounge-visual", *LAUNCH_DIR)
        }
        else { vis_path }
    };

    // only one player is allowed to run
    if audio_cmd::<usize>(PlayerCommand::ProcessID, true).is_ok() {
        LOG.line(LogLevel::Error, "Music Lounge is already running!", true);
        process::exit(101);
    }

    LOG.report_panics(true);
    LOG.line_basic("Starting up Music Lounge background process...", true);
    fs::create_dir_all(&*TMP_DIR).unwrap_or_default();
    lazy_static::initialize(&LIBRARY);
    let mut wave_id = 0;
    let mut matrix_id = 0;
    let mut info_id = 0;
    let mut system = System::new_all();
    system.refresh_all();

    // launch the wave visualizer process
    if CONFIG.show_wave_vis && !check_vis(&system, "wave") {
        wave_id = launch_vis("Wave Visualizer", "wave", &vis_prog);
    }

    // launch the matrix visualizer process
    if CONFIG.show_matrix_vis && !check_vis(&system, "matrix") {
        matrix_id = launch_vis("Matrix Visualizer", "matrix", &vis_prog);
    }

    // launch the info process
    if CONFIG.show_info && !check_vis(&system, "info") {
        info_id = launch_vis("Info", "info", &vis_prog);
    }

    system.refresh_all();

    // if Ctrl-C is pressed
    ctrlc::set_handler(move || {
        LOG.line_basic("Shutting down!", true);
        if wave_id != 0 {
            utils::run_command("kill", false, ["-2", wave_id.to_string().as_str()]);
        }
        if matrix_id != 0 {
            utils::run_command("kill", false, ["-2", matrix_id.to_string().as_str()]);
        }
        if info_id != 0 {
            utils::run_command("kill", false, ["-2", info_id.to_string().as_str()]);
        }

        PLAYLISTS.read().unwrap().save();
        process::exit(0);
    }).expect("Error setting Ctrl-C handler");

    daemon::spawn();
}

fn audio_cmd<T: for <'de> serde::Deserialize<'de>>(cmd: PlayerCommand, silent: bool) -> PlayerResult<T> {
    let socket_file = format!("{}/socket", *TMP_DIR);
    match UnixStream::connect(socket_file) {
        Ok(mut stream) => {
            let encoded = bincode::serialize(&cmd).expect("What went wrong?!");
            stream.write_all(&encoded).expect("Unable to write to socket!");
            stream.shutdown(Shutdown::Write).expect("What went wrong?!");
            let buffer = BufReader::new(&stream);
            let encoded: Vec<u8> = buffer.bytes().map(|r| r.unwrap_or(0)).collect();
            Ok(bincode::deserialize(&encoded).expect("What went wrong?!"))
        }

        Err(why) => {
            if !silent {
                LOG.line(LogLevel::Error, format!("Unable to connect to socket: {why}"), true);
            }

            Err(why.to_string())
        }
    }
}

fn check_vis(system: &System, name: &str) -> bool {
    for proc in system.processes().values() {
        let cmdline = proc.cmd();
        if let Some(vis_name) = cmdline.get(0) {
            if let Some(exe) = proc.exe() {
                let exe_str = exe.display().to_string();
                if vis_name == name && exe_str.contains("mlounge-visual") {
                    return true;
                }
            }
        }
    }

    false
}

fn build_music_lib(rescan: bool) -> Library {
    let listener = create_socket();
    listener.set_nonblocking(true).unwrap();
    let (snd, rcv) = bounded::<()>(1);
    let scan_thread = thread::spawn(move || {
        let lib = Library::load(true, rescan, &CONFIG.music_dir, &LOG);
        snd.send(()).unwrap();
        thread::park();
        lib
    });

    while rcv.is_empty() {
        if let Ok((mut stream, _)) = listener.accept() {
            thread::spawn(move || {
                let buffer = BufReader::new(&stream);
                let encoded: Vec<u8> = buffer.bytes().map(Result::unwrap_or_default).collect();
                let command: PlayerCommand = bincode::deserialize(&encoded).expect("Error parsing request!");

                if command == PlayerCommand::GetLibrary {
                    send_val::<Option<Library>>(&mut stream, &None);
                }
            });
        }
    }

    scan_thread.thread().unpark();
    scan_thread.join().unwrap()
}

// create a unix socket to communicate with clients
fn create_socket() -> UnixListener {
    fs::remove_file(&*SOCKET).unwrap_or_default();
    UnixListener::bind(&*SOCKET).expect("What went wrong?!")
}

// send a value to a client
fn send_val<V: serde::Serialize + for <'de> serde::Deserialize<'de> + ?Sized>(stream: &mut UnixStream, val: &V) {
    let encoded = bincode::serialize(val).expect("What went wrong?!");
    if let Err(why) = stream.write_all(&encoded) {
        LOG.line(LogLevel::Error, format!("Unable to write to socket: {why}"), false);
    };

    stream.shutdown(Shutdown::Write).expect("What went wrong?!");
}

fn launch_vis(name: &str, id: &str, vis_prog: &str) -> u32 {
    #[cfg(not(debug_assertions))]
    let new_vis_prog = format!("/usr/bin/mlounge-{id}");

    #[cfg(debug_assertions)]
    let new_vis_prog = {
        let vis_path = format!("{}/mlounge-{id}", *LAUNCH_DIR);
        if fs::read(&vis_path).is_err() {
            format!("{}/target/debug/mlounge-{id}", *LAUNCH_DIR)
        }
        else { vis_path }
    };

    let prog = if fs::read(&new_vis_prog).is_err() {
        vis_prog
    }
    else { &new_vis_prog };

    let proc = utils::spawn_process(prog, false, false, [id]);
    let vis_pid = proc.id();
    LOG.line_basic(format!("{name} process ID: {vis_pid}"), true);
    vis_pid
}