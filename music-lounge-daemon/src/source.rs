use std::{
    ops::{Deref, DerefMut},
    fs::File,
    time::Duration,
    io::BufReader
};
use mlounge_core::library::Song;
use rodio::{Sink, OutputStream, Decoder, OutputStreamHandle, Source};

thread_local! {
    static HANDLE: (OutputStream, OutputStreamHandle) = OutputStream::try_default().unwrap();
}

pub struct PlayerSource {
    sink: Sink,
    song: Option<Song>
}

impl PlayerSource {
    pub fn new() -> Self {
        let mut source = None;
        HANDLE.with(|(_stream, handle)| {
            let sink = Sink::try_new(&handle).unwrap();
            source = Some(PlayerSource {
                sink,
                song: None
            });
        });

        source.unwrap()
    }

    pub fn set_song(&mut self, song: &Song) {
        self.stop();
        let source = Decoder::new(BufReader::new(File::open(song.get_path()).unwrap())).unwrap();
        self.append(source);
        self.play();
        self.song = Some(song.clone());
    }

    pub fn seek(&mut self, time: u64) {
        self.stop();
        if let Some(ref song) = self.song {
            let source = Decoder::new(BufReader::new(File::open(song.get_path()).unwrap())).unwrap()
                .skip_duration(Duration::from_secs(time));

            self.append(source);
            self.play();
        }
    }

    pub fn stop(&mut self) {
        self.sink.stop();
        self.sink.clear();
    }
}

impl Deref for PlayerSource {
    type Target = Sink;
    fn deref(&self) -> &Sink { &self.sink }
}

impl DerefMut for PlayerSource {
    fn deref_mut(&mut self) -> &mut Sink { &mut self.sink }
}