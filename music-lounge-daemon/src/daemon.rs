use std::{
    process, thread::Builder as ThreadBuilder,
    sync::RwLock,
    io::{
        BufReader,
        Read
    }
};
use lazy_static::lazy_static;
use mlounge_core::{
    library::PlaylistStore,
    player::{
        PlayerCommand,
        PlayerStatus
    }
};
use crate::{
    PLAYER, LOG, CONFIG, LIBRARY,
    mpris::MprisController,
    create_socket,
    send_val
};

lazy_static! {
    pub static ref PLAYLISTS: RwLock<PlaylistStore> = RwLock::new(PlaylistStore::load(&CONFIG.music_dir, &LIBRARY.read().unwrap()));
}

#[allow(clippy::manual_flatten)]
pub fn spawn() {
    let mut listener = create_socket();

    // move to the next song when it ends
    ThreadBuilder::new().name("player-ctl".to_string()).spawn(|| {
        loop {
            if let Ok(mut player) = PLAYER.try_write() {
                player.auto_advance();
            }
        }
    }).expect("Why didn't the thread spawn?!");

    if CONFIG.use_mpris {
        ThreadBuilder::new().name("mpris-ctl".to_string()).spawn(|| {
            let mut mpris = MprisController::new();
            mpris.run();
        }).expect("Why didn't the thread spawn?!");
    }

    LOG.line_basic("Startup complete!", true);

    loop {
        for mut stream in listener.incoming().filter_map(Result::ok) {
            let buffer = BufReader::new(&stream);
            let encoded: Vec<u8> = buffer.bytes().map(Result::unwrap_or_default).collect();
            let command: PlayerCommand = bincode::deserialize(&encoded).expect("Error parsing request!");

            if command.is_mut() {
                let mut player = PLAYER.write().expect("What went wrong?!");
                match command {
                    PlayerCommand::Load(playlist) => player.load_list(&playlist),
                    PlayerCommand::CycleRepeat => player.cycle_repeat(),
                    PlayerCommand::Play => player.play(),
                    PlayerCommand::Restart => player.restart(),
                    PlayerCommand::Next => player.next(),
                    PlayerCommand::Prev => player.prev(),
                    PlayerCommand::Resume => player.resume(),
                    PlayerCommand::Pause => player.pause(),
                    PlayerCommand::Stop => player.stop(),
                    PlayerCommand::Seek(time) => player.seek(time),

                    PlayerCommand::Shuffle => {
                        player.shuffle_queue();
                        player.find_pos();
                    }

                    PlayerCommand::SetPos(song) => {
                        player.set_pos(&song);
                        player.find_pos();
                    }

                    PlayerCommand::SetQueue(playlist) => {
                        player.queue = playlist;
                        player.find_pos();
                    }

                    _ => panic!("Invalid player action!")
                }
            }
            else {
                let player = PLAYER.read().unwrap();

                match command {
                    PlayerCommand::ProcessID => {
                        let id = process::id() as usize;
                        send_val(&mut stream, &id);
                    }

                    PlayerCommand::CurrentTime => {
                        let time = player.cur_time_secs();
                        send_val(&mut stream, &time);
                    }

                    PlayerCommand::Status => {
                        let status = PlayerStatus {
                            stopped: player.is_stopped(),
                            paused: player.is_paused(),
                            position: player.position,
                            repeat_mode: player.repeat,
                            state: player.state,
                            song_id: player.song.song_id()
                        };

                        send_val(&mut stream, &status);
                    }

                    PlayerCommand::GetQueue => send_val(&mut stream, &player.queue),
                    PlayerCommand::GetPlaylists => send_val(&mut stream, &*(PLAYLISTS.read().unwrap())),

                    PlayerCommand::UpdatePlaylists(new_lists) => {
                        let mut playlists = PLAYLISTS.write().unwrap();
                        *playlists = new_lists;
                        playlists.gen_thumbnails();
                        playlists.save();
                    }

                    PlayerCommand::GetLibrary => {
                        let lib = Some(LIBRARY.read().unwrap().clone());
                        send_val(&mut stream, &lib);
                    }

                    // break from the event loop for library rescanning
                    PlayerCommand::RescanLibrary => break,

                    _ => panic!("Invalid player action!")
                }
            }
        }

        // rescan the music library and then restart the event loop
        *(LIBRARY.write().unwrap()) = crate::build_music_lib(true);
        listener = create_socket();
    }
}