use super::*;
use std::{
    collections::HashMap,
    sync::Arc,
    time::Instant
};
use dbus::{
    message::SignalArgs,
    arg::{RefArg, Variant, Append, IterAppend},
    ffidisp::{Connection, BusType, NameFlag, stdintf::org_freedesktop_dbus::PropertiesPropertiesChanged},
    MethodErr, Path
};
use dbus_tree::{Access, Factory};
use mlounge_core::player::{PlayerState, RepeatMode};

type Metadata = HashMap<String, Variant<Box<dyn RefArg>>>;

//TODO: Figure out how to reimplement this using zbus
pub struct MprisController(Arc<Connection>);

impl MprisController {
    pub fn new() -> MprisController {
        // set up the dbus connection
        let conn = Arc::new(Connection::get_private(BusType::Session).expect("Failed to connect to dbus"));
        conn.register_name("org.mpris.MediaPlayer2.mlounge", NameFlag::ReplaceExisting as u32).expect("Failed to register dbus player name");

        // set up the media controls
        let factory = Factory::new_fn::<()>();

        let property_canquit = factory
            .property::<bool, _>("CanQuit", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_canraise = factory
            .property::<bool, _>("CanRaise", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, false));

        let property_cansetfullscreen = factory
            .property::<bool, _>("CanSetFullscreen", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, false));

        let property_hastracklist = factory
            .property::<bool, _>("HasTrackList", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, false));

        let property_identity = factory
            .property::<&'static str, _>("Identity", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, "Music Lounge"));

        let property_icon = factory
            .property::<&'static str, _>("DesktopEntry", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, "mlounge"));

        let property_urischemes = factory
            .property::<Vec<&'static str>, _>("SupportedUriSchemes", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, vec!["file"]));

        let property_mimetypes = factory
            .property::<Vec<&'static str>, _>("SupportedMimeTypes", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, vec![""]));

        // https://specifications.freedesktop.org/mpris-spec/latest/Media_Player.html
        let interface = factory
            .interface("org.mpris.MediaPlayer2", ())
            .add_p(property_canquit)
            .add_p(property_cansetfullscreen)
            .add_p(property_canraise)
            .add_p(property_hastracklist)
            .add_p(property_identity)
            .add_p(property_urischemes)
            .add_p(property_mimetypes)
            .add_p(property_icon);

        let property_playbackstatus = factory
            .property::<String, _>("PlaybackStatus", ())
            .access(Access::Read)
            .on_get(|iter, _| {
                let player = PLAYER.read().unwrap();
                append_val(iter, String::from(
                    match player.state {
                        PlayerState::Playing => "Playing",
                        PlayerState::Paused(_) => "Paused",
                        PlayerState::Stopped => "Stopped",
                    }
                ))
            });

        let property_shuffle = factory
            .property::<bool, _>("Shuffle", ())
            .access(Access::ReadWrite)
            .on_get(|iter, _| append_val(iter, false))
            .on_set(|_, _| {
                let mut player = PLAYER.write().unwrap();
                player.shuffle_queue();
                player.find_pos();
                Ok(())
            });

        let property_loopstatus = factory
            .property::<&'static str, _>("LoopStatus", ())
            .access(Access::ReadWrite)
            .on_get(|iter, _| {
                let player = PLAYER.read().unwrap();
                append_val(iter,
                    match player.repeat {
                        RepeatMode::One => "Track",
                        RepeatMode::All => "Playlist",
                        RepeatMode::Once => "None",
                    }
                )
            })
            .on_set(|iter, _| {
                let mut player = PLAYER.write().unwrap();
                let setting = match iter.get().unwrap_or_default() {
                    "Track" => RepeatMode::One,
                    "Playlist" => RepeatMode::All,
                    "None" => RepeatMode::Once,
                    _ => unreachable!("How did I get here?")
                };
                player.repeat = setting;
                Ok(())
            });

        let property_metadata = factory
            .property::<Metadata, _>("Metadata", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, gen_metadata()));

        let property_volume = factory
            .property::<f64, _>("Volume", ())
            .on_set(|_, _| Ok(()))
            .on_get(|iter, _| append_val(iter, 100.));

        let property_rate = factory
            .property::<f64, _>("Rate", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, 1.));

        let property_minrate = factory
            .property::<f64, _>("MinimumRate", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, 1.));

        let property_maxrate = factory
            .property::<f64, _>("MaximumRate", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, 1.));

        let property_canplay = factory
            .property::<bool, _>("CanPlay", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_canpause = factory
            .property::<bool, _>("CanPause", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_canseek = factory
            .property::<bool, _>("CanSeek", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_cancontrol = factory
            .property::<bool, _>("CanControl", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_cangonext = factory
            .property::<bool, _>("CanGoNext", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_cangoprevious = factory
            .property::<bool, _>("CanGoPrevious", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_cangoforward = factory
            .property::<bool, _>("CanGoForward", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_canrewind = factory
            .property::<bool, _>("CanRewind", ())
            .access(Access::Read)
            .on_get(|iter, _| append_val(iter, true));

        let property_position = factory.property::<i64, _>("Position", ())
            .access(Access::Read)
            .on_get(|iter, _| {
                let player = PLAYER.read().unwrap();
                let time = player.cur_time_secs() * 1_000_000;
                append_val(iter, time as i64)
            });

        let method_playpause = factory
            .method("PlayPause", (), |m| {
                let mut player = PLAYER.write().unwrap();
                if player.is_paused() {
                    player.resume();
                }
                else {
                    player.pause();
                }
                Ok(vec![m.msg.method_return()])
            });

        let method_play = factory
            .method("Play", (), |m| {
                player_cmd(PlayerCommand::Resume);
                Ok(vec![m.msg.method_return()])
            });

        let method_pause = factory
            .method("Pause", (), |m| {
                player_cmd(PlayerCommand::Pause);
                Ok(vec![m.msg.method_return()])
            });

        let method_stop = factory
            .method("Stop", (), |m| {
                player_cmd(PlayerCommand::Stop);
                Ok(vec![m.msg.method_return()])
            });

        let method_next = factory
            .method("Next", (), |m| {
                player_cmd(PlayerCommand::Next);
                Ok(vec![m.msg.method_return()])
            });

        let method_previous = factory
            .method("Previous", (), |m| {
                player_cmd(PlayerCommand::Prev);
                Ok(vec![m.msg.method_return()])
            });

        let method_set_pos = factory.method("SetPosition", (), |m| {
            let (_, micros) = m.msg.get2::<Path, i64>();
            let time = (micros.unwrap_or(0) / 1_000_000) as u64;
            player_cmd(PlayerCommand::Seek(time));
            player_cmd(PlayerCommand::Resume);
            Ok(vec![m.msg.method_return()])
        });

        let method_seek = factory.method("Seek", (), |m| {
            let player = PLAYER.read().unwrap();
            let time_micros = (player.cur_time_secs() * 1_000_000) as i64;
            let offset = m.msg.get1::<i64>().unwrap_or(0);
            let time = (time_micros + offset) as u64;
            drop(player);
            player_cmd(PlayerCommand::Seek(time / 1_000_000));
            player_cmd(PlayerCommand::Resume);
            Ok(vec![m.msg.method_return()])
        });

        // https://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html
        let interface_player = factory
            .interface("org.mpris.MediaPlayer2.Player", ())
            .add_p(property_playbackstatus)
            .add_p(property_loopstatus)
            .add_p(property_rate)
            .add_p(property_metadata)
            .add_p(property_position)
            .add_p(property_volume)
            .add_p(property_minrate)
            .add_p(property_maxrate)
            .add_p(property_cangonext)
            .add_p(property_cangoprevious)
            .add_p(property_canplay)
            .add_p(property_canpause)
            .add_p(property_canseek)
            .add_p(property_cancontrol)
            .add_p(property_shuffle)
            .add_p(property_cangoforward)
            .add_p(property_canrewind)
            .add_m(method_playpause)
            .add_m(method_play)
            .add_m(method_pause)
            .add_m(method_stop)
            .add_m(method_next)
            .add_m(method_previous)
            .add_m(method_set_pos)
            .add_m(method_seek);

        let tree = factory.tree(()).add(
            factory.object_path("/org/mpris/MediaPlayer2", ())
                .introspectable()
                .add(interface)
                .add(interface_player),
        );

        tree.set_registered(&conn, true).expect("failed to register tree");
        conn.add_handler(tree);
        MprisController(conn)
    }

    pub fn run(&mut self) {
        let mut timer = Instant::now();

        loop {
            let player = PLAYER.read().unwrap();
            let mut cur_album = player.song.album.clone();
            let mut cur_name = player.song.name.clone();
            let mut cur_state = player.state;
            drop(player);
            while timer.elapsed().as_secs() < 60 {
                if self.0.incoming(200).next().is_some() { }
                let player = PLAYER.read().unwrap();
                let cur_time = player.cur_time_secs() * 1_000_000;
                let album = player.song.album.clone();
                let name = player.song.name.clone();
                let state = player.state;
                let mut changed = PropertiesPropertiesChanged {
                    interface_name: "org.mpris.MediaPlayer2.Player".to_string(),
                    ..Default::default()
                };

                if state != cur_state || album != cur_album || name != cur_name {
                    changed.changed_properties.insert(
                        "Metadata".to_string(),
                        Variant(Box::new(gen_metadata())),
                    );

                    changed.changed_properties
                        .insert("PlaybackStatus".to_string(), Variant(Box::new(String::from(
                            match player.state {
                                PlayerState::Playing => "Playing",
                                PlayerState::Paused(_) => "Paused",
                                PlayerState::Stopped => "Stopped",
                            }
                        ))));

                    changed.changed_properties
                        .insert("Position".to_string(), Variant(Box::new(cur_time)));

                    cur_name = name;
                    cur_album = album;
                    cur_state = state;
                }

                self.0.send(changed.to_emit_message(&Path::new("/org/mpris/MediaPlayer2").unwrap())).unwrap();
            }

            timer = Instant::now();
        }
    }
}

fn player_cmd(cmd: PlayerCommand) {
    let mut player = PLAYER.write().unwrap();
    match cmd {
        PlayerCommand::Next => player.next(),
        PlayerCommand::Prev => player.prev(),
        PlayerCommand::Resume => player.resume(),
        PlayerCommand::Pause => player.pause(),
        PlayerCommand::Stop => player.stop(),
        PlayerCommand::Seek(time) => player.seek(time),
        _ => panic!("Invalid command: {cmd:?}")
    }
}

fn append_val<T: Append>(iter: &mut IterAppend, val: T) ->  Result<(), MethodErr> {
    iter.append(val);
    Ok(())
}

// generate the metadata of the currently playing song
fn gen_metadata() -> Metadata {
    let player = PLAYER.read().unwrap();
    let mut meta = Metadata::new();
    let song = &player.song;

    // song number
    meta.insert("mpris:trackid".to_string(), Variant(Box::new(Path::new(format!("/org/mlounge/{}", song.song_id())).unwrap())));

    // song length (in microseconds)
    meta.insert("mpris:length".to_string(), Variant(Box::new(song.duration * 1_000_000)));

    // album art path
    let path = format!("file://{}", song.get_art_path().unwrap_or_default());
    meta.insert("mpris:artUrl".to_string(), Variant(Box::new(path)));

    // album name
    meta.insert("xesam:album".to_string(), Variant(Box::new(song.album.clone())));

    // artist name
    meta.insert("xesam:artist".to_string(), Variant(Box::new(vec![song.artist.clone()])));

    // song name
    meta.insert("xesam:title".to_string(), Variant(Box::new(song.name.to_string())));

    meta
}