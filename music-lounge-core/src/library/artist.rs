use super::{Album, Song, PlaylistAddable};
use std::borrow::Cow;

/// Artist info
/// 
/// Contains information about an artist
#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Artist {
    /// What is the artist's name?
    pub name: String,

    /// What albums belong to them?
    pub albums: Vec<Album>
}

#[cfg(feature = "daemon")]
impl Artist {
    pub (super) fn new(name: &str) -> Self {
        Artist {
            name: name.to_owned(),
            albums: Vec::new()
        }
    }

    pub (super) fn add_album(&mut self, album: &Album) { self.albums.push(album.clone()); }
    pub (super) fn sort_albums(&mut self) { self.albums.sort_unstable(); }
}

impl PlaylistAddable for Artist {
    fn song_list(&self) -> Cow<[Song]> {
        let mut all_songs = Vec::new();
        for album in &self.albums {
            for song in &album.songs {
                all_songs.push(song.clone());
            }
        }
        Cow::from(all_songs)
    }
}