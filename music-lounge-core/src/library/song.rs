use super::{
    PlaylistAddable,
    album::ALBUM_RE
};
use std::{
    fs,
    borrow::Cow,
    hash::{Hash, Hasher},
    path::PathBuf,
    cmp::Ordering,
    collections::hash_map::DefaultHasher
};
use lofty::{read_from_path, TaggedFileExt};
use crate::{TMP_DIR, album_colors::RawColor};

#[cfg(feature = "piston")]
use crate::PistonColor;

#[cfg(feature = "cursive")]
use {
    crate::{
        config::ThemeConfig,
        album_colors::FromRawColor
    },
    cursive_core::theme::{Color, Theme, PaletteColor},
    rust_utils::config::Config
};

#[cfg(any(feature = "piston", feature = "daemon", feature = "cursive"))]
use crate::album_colors;

/// Song info
///
/// Contains information about a song
#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize, Eq, PartialEq, Debug)]
pub struct Song {
    /// What is the song's name?
    pub name: String,

    /// What album is it from?
    pub album: String,

    /// Who created it?
    pub artist: String,

    /// How long is it (in seconds)?
    pub duration: u64,

    /// Track number in the album
    pub track_num: u32,

    // the song's path relative to the music directory
    path: String,

    // color for the song name in the playlist editor
    name_color: RawColor,

    // color for the artist name in the playlist editor
    artist_color: RawColor,

    // string used for the song ID
    hash_string: String
}

impl Song {
    /// Creates an empty song
    ///
    /// Mostly used as a placeholder
    pub fn empty() -> Song {
        Song {
            name: String::new(),
            album: String::new(),
            artist: String::new(),
            duration: 0,
            track_num: 0,
            path: String::new(),
            name_color: (0, 0, 0),
            artist_color: (0, 0, 0),
            hash_string: String::new()
        }
    }

    /// Get the song duration as MM:SS
    pub fn get_duration_str(&self) -> String {
        let duration = self.duration.saturating_sub(1);
        let sec_rem = duration % 60;
        let minutes = (duration - sec_rem) / 60;
        format!("{minutes}:{sec_rem:02}")
    }

    /// Get the file path of the song
    pub fn get_path(&self) -> &str { &self.path }

    /// Get the path of the song artwork
    pub fn get_art_path(&self) -> Option<String> {
        if self.extract_artwork() {
            Some(format!("{}/{}", &*TMP_DIR, self.song_id()))
        }
        else {
            self.get_album_art_path()
        }
    }

    fn get_album_art_path(&self) -> Option<String> {
        let mut album_path = PathBuf::from(&self.path);
        album_path.pop();
        let file_list = fs::read_dir(&album_path).ok()?;
        for file in file_list {
            let name_t = file.unwrap().path().into_os_string();
            let name = name_t.to_str().expect("What went wrong?!");
            if ALBUM_RE.is_match(name) {
                return Some(name.to_string())
            }
        }

        None
    }

    /// Unique ID for this song
    pub fn song_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.hash_string.hash(&mut hasher);
        hasher.finish()
    }

    /// Get the colors of the song to use with piston
    #[cfg(feature = "piston")]
    pub fn piston_colors(&self) -> Vec<PistonColor> {
        if let Some(path) = self.get_art_path() {
            album_colors::get_art_colors(&path)
        }
        else { vec![] }
    }

    /// Extracts the artwork to the platform's temp directory with
    /// the name being the song's ID
    ///
    /// Returns `true` if extraction was successful
    pub fn extract_artwork(&self) -> bool {
        if let Ok(song_file) = read_from_path(&self.path) {
            let pic_path = format!("{}/{}", *TMP_DIR, self.song_id());
            if fs::read(&pic_path).is_ok() {
                return true;
            }
            else if let Some(tag) = song_file.first_tag() {
                if let Some(artwork) = tag.pictures().get(0) {
                    let data = artwork.data();

                    if let Some(album_art_path) = self.get_album_art_path() {
                        if let Ok(album_art) = fs::read(album_art_path) {
                            if same_images(data, &album_art) {
                                return false;
                            }
                        }
                    }

                    return fs::write(pic_path, data).is_ok();
                }
            }
        }

        false
    }

    #[cfg(feature = "daemon")]
    pub (super) fn new(name: &str, album: &str, artist: &str, duration: u64, track_num: u32, path: &str) -> Song {
        let mut new_song = Song {
            name: name.to_string(),
            album: album.to_string(),
            artist: artist.to_string(),
            duration,
            track_num,
            path: path.to_string(),
            artist_color: (0, 0, 0),
            name_color: (0, 0, 0),
            hash_string: format!("{name}:{album}:{artist}")
        };

        if let Some(path) = new_song.get_art_path() {
            (new_song.artist_color, new_song.name_color) = album_colors::get_list_colors(&path);
        }

        new_song
    }
}

#[cfg(feature = "cursive")]
impl Song {
    /// Get the colors of the song to use with `cursive`
    pub fn cursive_album_colors(&self) -> Vec<Color> {
        if let Some(path) = self.get_art_path() {
            album_colors::get_art_colors(&path)
        }
        else { vec![] }
    }
    
    /// Get a theme config based on this song's artwork
    pub fn tui_theme(&self) -> Theme {
        if let Some(path) = self.get_art_path() {
            album_colors::get_full_theme(&path).to_cursive_theme()
        }
        else { ThemeConfig::load().to_cursive_theme() }
    }

    /// Returns the artist and name colors for use in the playlist editor
    pub fn list_colors(&self) -> (Color, Color) {
        if self.artist_color == (0, 0, 0) {
            let theme = self.tui_theme();
            (theme.palette[PaletteColor::TitlePrimary], theme.palette[PaletteColor::Primary])
        }
        else {
            (Color::from_raw(self.artist_color), Color::from_raw(self.name_color))
        }
    }
}

impl PlaylistAddable for Song {
    fn song_list(&self) -> Cow<[Song]> { Cow::from(vec![self.clone()]) }
}

impl Ord for Song {
    fn cmp(&self, other: &Song) -> Ordering { self.track_num.cmp(&other.track_num) }
}

impl PartialOrd for Song {
    fn partial_cmp(&self, other: &Song) -> Option<Ordering> { Some(self.cmp(other)) }
}

fn same_images(song_art: &[u8], album_art: &[u8]) -> bool {
    if song_art.len() == album_art.len() {
        for (idx, byte) in song_art.iter().copied().enumerate() {
            if byte != album_art[idx] {
                return false;
            }
        }

        return true;
    }

    false
}