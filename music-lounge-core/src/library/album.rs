use super::{Song, PlaylistAddable};
use lazy_static::lazy_static;
use regex::Regex;
use std::{
    borrow::Cow,
    cmp::Ordering, path::PathBuf, fs
};

#[cfg(feature = "cursive")]
use {
    cursive_core::theme::{Color, PaletteColor},
    crate::{
        album_colors::FromRawColor,
        config::ThemeConfig
    }
};

#[cfg(feature = "daemon")]
use crate::album_colors;

use crate::album_colors::RawColor;

lazy_static! {
    pub (super) static ref ALBUM_RE: Regex = Regex::new(r"[Ff]older\.[a-zA-Z]*").expect("Invalid Regex!");
}

/// Album info
/// 
/// Contains information about an album
#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize, Eq)]
pub struct Album {
    /// What is the album's name?
    pub name: String,

    /// Who created it?
    pub artist: String,

    /// What are the songs in it?
    pub songs: Vec<Song>,

    /// The year the album was created
    pub year: u32,

    title_color: RawColor
}

impl Album {
    /// Is this album a single?
    pub fn is_single(&self) -> bool {
        if self.songs.len() == 1 {
            let song = &self.songs[0];
            self.name.contains(&song.name)
        }
        else { false }
    }

    /// Get the artwork path for the album
    pub fn get_art_path(&self) -> Option<String> {
        let mut album_path = PathBuf::from(self.songs.get(0)?.get_path());
        album_path.pop();
        let file_list = fs::read_dir(&album_path).ok()?;
        for file in file_list {
            let name_t = file.unwrap().path().into_os_string();
            let name = name_t.to_str().expect("What went wrong?!");
            if ALBUM_RE.is_match(name) {
                return Some(name.to_string())
            }
        }

        None
    }

    #[cfg(feature = "cursive")]
    pub fn title_color(&self) -> Color {
        if self.title_color == (0, 0, 0) {
            let theme = ThemeConfig::default().to_cursive_theme();
            theme.palette[PaletteColor::TitlePrimary]
        }
        else {
            Color::from_raw(self.title_color)
        }
    }
}

#[cfg(feature = "daemon")]
impl Album {
    pub (super) fn new(name: &str, artist: &str, year: u32) -> Album {
        Album {
            name: name.to_owned(),
            artist: artist.to_owned(),
            songs: Vec::new(),
            year,
            title_color: (0, 0, 0)
        }
    }

    pub (super) fn add_song(&mut self, song: &Song) { self.songs.push(song.clone()); }

    pub (super) fn sort_songs(&mut self) {
        self.songs.sort_unstable();

        if let Some(art_path) = self.get_art_path() {
            (self.title_color, _) = album_colors::get_list_colors(&art_path);
        }
    }
}

impl Ord for Album {
    fn cmp(&self, other: &Album) -> Ordering { self.year.cmp(&other.year) }
}

impl PartialOrd for Album {
    fn partial_cmp(&self, other: &Album) -> Option<Ordering> { Some(self.cmp(other)) }
}

/// For the sake of comparing release years, 2 albums
/// are "equal" to each other if their years are equal
impl PartialEq for Album {
    fn eq(&self, other: &Album) -> bool { self.year == other.year }
}

impl PlaylistAddable for Album {
    fn song_list(&self) -> Cow<[Song]> { Cow::from(&self.songs) }
}