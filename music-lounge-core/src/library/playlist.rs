use super::Song;
use std::{
    fs,
    borrow::Cow,
    slice::{Iter, IterMut},
    
};
use rand::seq::SliceRandom;
use rust_utils::config::Config;
use crate::config::MLConfig;

#[cfg(feature = "client")]
use crate::player::{PlayerCommand, PlayerResult, audio_cmd};

#[cfg(feature = "daemon")]
use {
    super::Library,
    std::{
        fmt::Write,
        path::PathBuf,
        collections::HashSet
    }
};

/// The playlist database used by Music Lounge
#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize, Eq, Debug)]
pub struct PlaylistStore {
    pub lists: Vec<Playlist>,
    music_dir: String
}

impl PlaylistStore {
    /// Iterate over the `PlaylistStore`
    pub fn iter(&self) -> Iter<Playlist> {
        self.lists.iter()
    }

    /// Iterate mutably over the `PlaylistStore`
    pub fn iter_mut(&mut self) -> IterMut<Playlist> {
        self.lists.iter_mut()
    }
}

#[cfg(feature = "client")]
impl PlaylistStore {
    /// Add a new playlist to the database
    pub fn add_playlist(&mut self, playlist: Playlist) {
        self.lists.push(playlist);
        self.update().unwrap();
    }

    /// Remove a playlist from the database
    pub fn remove_playlist(&mut self, name: &str) {
        self.lists.retain(|list| list.name != name);
        self.update().unwrap();
    }
    
    /// Rename a playlist in the database
    pub fn rename_playlist(&mut self, name: &str, new_name: &str) {
        let mut new_list = Playlist::new("_");
        for list in self.iter_mut() {
            if list.name == name {
                new_list = list.clone();
                new_list.name = new_name.to_string();
                break;
            }
        }

        self.remove_playlist(name);
        self.add_playlist(new_list);
    }

    /// Update the background process' playlist database
    pub fn update(&self) -> PlayerResult<()> {
        audio_cmd(None, PlayerCommand::UpdatePlaylists(self.clone()))
    }
}

#[cfg(feature = "daemon")]
impl PlaylistStore {
    /// Load in the playlist database or creates a new one if it doesn't exist
    pub fn load(music_dir: &str, library: &Library) -> PlaylistStore {
        fs::create_dir_all(format!("{music_dir}/.mucl")).unwrap_or(());
        let encoded = if let Ok(ps) = fs::read(format!("{music_dir}/.mucl/playlists")) { ps }
        else {
            let mut store = PlaylistStore {
                lists: vec![],
                music_dir: music_dir.to_string()
            };

            for file in fs::read_dir(format!("{music_dir}/Playlists")).unwrap() {
                let name = file.unwrap().path().display().to_string();
                if let Some(playlist) = Playlist::import_m3u(&name, library) {
                    store.lists.push(playlist);
                }
            }

            let encoded = bincode::serialize(&store).unwrap();
            fs::write(format!("{music_dir}/.mucl/playlists"), encoded).unwrap();
            return store;
        };

        if let Ok(mut lists) = bincode::deserialize::<PlaylistStore>(&encoded) {
            lists.lists.sort_unstable_by_key(|list| list.name.to_lowercase());
            for list in &mut lists.lists {
                list.m3u_init(library);
            }
            lists
        }
        else {
            let mut store = PlaylistStore {
                lists: vec![],
                music_dir: music_dir.to_string()
            };

            for file in fs::read_dir(format!("{music_dir}/Playlists")).unwrap() {
                let name = file.unwrap().path().display().to_string();
                if let Some(playlist) = Playlist::import_m3u(&name, library) {
                    store.lists.push(playlist);
                }
            }

            let encoded = bincode::serialize(&store).expect("What went wrong?!");
            fs::write(format!("{music_dir}/.mucl/playlists"), encoded).expect("What went wrong?!");
            store
        }
    }

    /// Save the playlist database
    pub fn save(&self) {
        let playlists = format!("{}/Playlists", self.music_dir);
        fs::remove_dir_all(&playlists).unwrap();
        fs::create_dir_all(&playlists).unwrap();

        for list in self.iter() {
            list.save_m3u(&self.music_dir);
        }
        let encoded = bincode::serialize(&self).expect("What went wrong?!");
        fs::write(format!("{}/.mucl/playlists", self.music_dir), encoded).expect("What went wrong?!");
    }

    /// Cache the song artwork of all the songs in a playlist under
    /// `<music library directory>/.mucl/playlist_thumbs/<playlist name>`
    pub fn gen_thumbnails(&mut self) {
        for playlist in &mut self.lists {
            let thumb_dir = format!("{}/.mucl/playlist_thumbs/{}", self.music_dir, playlist.name);
            if fs::read_dir(&thumb_dir).is_err() || playlist.refresh_thumb_cache {
                fs::remove_dir_all(&thumb_dir).unwrap_or_default();
                fs::create_dir_all(&thumb_dir).unwrap();
                let mut unique_imgs: HashSet<String> = HashSet::new();

                for song in playlist.iter() {
                    if let Some(image) = song.get_art_path() {
                        if unique_imgs.insert(image.clone()) {
                            fs::copy(&image, format!("{thumb_dir}/{}", song.song_id())).unwrap();
                        }
                    }
                }

                playlist.refresh_thumb_cache = false;
            }
        }

        self.save();
    }
}

impl PartialEq for PlaylistStore {
    fn eq(&self, other: &Self) -> bool {
        self.music_dir == other.music_dir
    }
}

/// Playlist in Music Lounge
#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize, Eq, PartialEq, Debug)]
pub struct Playlist {
    /// The playlist's name (will be "_" if it is the play queue)
    pub name: String,

    /// The songs that the playlist has
    pub songs: Vec<Song>,

    // does the thumbnail cache need reloaded?
    refresh_thumb_cache: bool
}

impl Playlist {
    /// Create a new `Playlist`
    pub fn new(name: &str) -> Playlist {
        Playlist {
            name: name.to_string(),
            songs: Vec::new(),
            refresh_thumb_cache: true
        }
    }

    /// Add items to the playlist
    pub fn add_items<I: PlaylistAddable>(&mut self, items: &I) {
        let songs_to_add = items.song_list().to_vec();
        for song in songs_to_add {
            for p_song in self.iter() {
                if *p_song == song {
                    return;
                }
            }
            self.songs.push(song.clone());
        }

        self.refresh_thumb_cache = true;
    }

    /// Rename the playlist
    pub fn rename(&mut self, new_name: &str) {
        self.name = new_name.to_string();
        self.refresh_thumb_cache = true;
    }

    /// Remove a song by reference
    pub fn remove_song(&mut self, song: &Song) {
        self.songs.retain(|s| *s != *song);
        self.refresh_thumb_cache = true;
    }

    /// Shuffles the playlist
    pub fn shuffle(&mut self, first: usize) {
        let first_song = self.songs.remove(first);
        let mut rand = rand::thread_rng();
        let mut new_songs = vec![first_song];
        self.songs.shuffle(&mut rand);
        for song in self.iter() {
            new_songs.push(song.clone());
        }
        self.songs = new_songs;
    }

    /// Move the selected song index up or down in the playlist
    pub fn move_item(&mut self, idx: usize, up: bool) {
        let sel_song = self.songs[idx].clone();
        let list_len = self.songs.len() - 1;
        if (idx == 0 && up) || (idx >= list_len && !up) { return; }
        
        self.remove_song(&sel_song);
        if up {
            self.songs.insert(idx - 1, sel_song);
        }
        else {
            self.songs.insert(idx + 1, sel_song);
        }

        self.refresh_thumb_cache = false;
    }

    // save the list to its M3U file
    #[cfg(feature = "daemon")]
    fn save_m3u(&self, music_dir: &str) {
        let playlists = format!("{music_dir}/Playlists");
        fs::create_dir_all(&playlists).unwrap_or(());
        let mut m3u = String::from("#EXTM3U\n");
        for song in &self.songs {
            writeln!(m3u, "{}", song.get_path()).unwrap();
        }
        let name = self.name.replace('/', "~");
        fs::write(format!("{playlists}/{name}.m3u"), m3u).expect("What went wrong?!");
    }

    /// Get 4 images that can be used as a thumbnail for the playlist
    pub fn get_disp_images(&self) -> Option<[String; 4]> {
        let thumb_dir = format!("{}/.mucl/playlist_thumbs/{}", MLConfig::load().music_dir, self.name);
        let mut images: Vec<String> = Vec::new();
        if let Ok(dir) = fs::read_dir(&thumb_dir) {
            for file in dir.filter_map(Result::ok) {
                images.push(file.path().to_str().unwrap().to_string())
            }
        }

        if images.len() < 4 {
            return None;
        }

        let mut rand = rand::thread_rng();
        let mut strings = [String::new(), String::new(), String::new(), String::new()];
        for (i, image) in images.choose_multiple(&mut rand, 4).enumerate() {
            strings[i] = image.clone();
        }

        Some(strings)
    }

    /// Iterate over the playlist's songs
    pub fn iter(&self) -> Iter<Song> {
        self.songs.iter()
    }
}

#[cfg(feature = "daemon")]
impl Playlist {
    // initialize a playlist from its M3U file
    fn m3u_init(&mut self, library: &Library) {
        let config = MLConfig::load();
        if let Ok(file) = fs::read_to_string(format!("{}/Playlists/{}.m3u", config.music_dir, self.name.replace('/', "~"))) {
            let mut lines = file.lines();
            lines.next().unwrap();
            self.songs.clear();
            for line in lines {
                if let Some(song) = library.find_song(line) {
                    self.songs.push(song);
                }
            }
        }
    }

    // import from M3U
    fn import_m3u(m3u: &str, library: &Library) -> Option<Playlist> {
        let path = PathBuf::from(m3u);
        let name = path.file_name()?.to_str()?.replace('~', "/");
        let file = fs::read_to_string(m3u).ok()?;
        let mut lines = file.lines();
        lines.next()?;
        let mut playlist = Playlist::new(&name[..name.len() - 4]);
        for line in lines {
            if let Some(song) = library.find_song(line) {
                playlist.songs.push(song);
            }
        }
        Some(playlist)
    }
}

impl PlaylistAddable for Playlist {
    fn song_list(&self) -> Cow<[Song]> { Cow::from(&self.songs) }
}

/// This is an item that can be added to a playlist
pub trait PlaylistAddable {
    /// The item as a list of songs
    fn song_list(&self) -> Cow<[Song]>;
}

impl PlaylistAddable for &[Song] {
    fn song_list(&self) -> Cow<[Song]> { Cow::from(*self) }
}

impl PlaylistAddable for Vec<Song> {
    fn song_list(&self) -> Cow<[Song]> { Cow::from(self) }
}