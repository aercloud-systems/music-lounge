use super::{Config, ConfigType};
use std::{env, collections::HashMap};
use cursive_core::theme::{
    Theme,
    Color as CursiveColor,
    BaseColor as CursiveBaseColor,
    Palette,
    PaletteColor
};
use serde_derive::{Deserialize, Serialize};

/// The configurable theme of music lounge
#[derive(Deserialize, Serialize, Clone, Copy)]
pub struct ThemeConfig {
    // general cursive theme
    primary: Color,
    title_primary: Color,
    secondary: Color,
    highlight: Color,
    highlight_txt: Color,
    highlight_inactive: Color,

    // theme for the custom views
    progress_bar: Color,
    now_playing_song: Color,
    now_playing_album: Color,
    now_playing_artist: Color,
    play_queue_playing_song: Color,
    play_queue_playing_album: Color,
    play_queue_playing_artist: Color,
    play_queue_playing_time: Color, 
    play_queue_song: Color,
    play_queue_album: Color,
    play_queue_artist: Color,
    play_queue_time: Color
}

impl ThemeConfig {
    /// Convert to theme for use with the `cursive` TUI library
    pub fn to_cursive_theme(self) -> Theme {
        let mut palette = Palette::default();
        palette[PaletteColor::Primary] = self.primary.to_cursive_color();
        palette[PaletteColor::TitlePrimary] = self.title_primary.to_cursive_color();
        palette[PaletteColor::Secondary] = self.secondary.to_cursive_color();
        palette[PaletteColor::Highlight] = self.highlight.to_cursive_color();
        palette[PaletteColor::HighlightText] = self.highlight_txt.to_cursive_color();
        palette[PaletteColor::HighlightInactive] = self.highlight_inactive.to_cursive_color();
        palette[PaletteColor::Background] = CursiveColor::TerminalDefault;
        palette[PaletteColor::View] = CursiveColor::TerminalDefault;
        palette.set_color("bar", self.progress_bar.to_cursive_color());
        palette.set_color("np_song", self.now_playing_song.to_cursive_color());
        palette.set_color("np_album", self.now_playing_album.to_cursive_color());
        palette.set_color("np_artist", self.now_playing_artist.to_cursive_color());
        palette.set_color("pqp_song", self.play_queue_playing_song.to_cursive_color());
        palette.set_color("pqp_album", self.play_queue_playing_album.to_cursive_color());
        palette.set_color("pqp_artist", self.play_queue_playing_artist.to_cursive_color());
        palette.set_color("pqp_time", self.play_queue_playing_time.to_cursive_color());
        palette.set_color("pq_song", self.play_queue_song.to_cursive_color());
        palette.set_color("pq_album", self.play_queue_album.to_cursive_color());
        palette.set_color("pq_artist", self.play_queue_artist.to_cursive_color());
        palette.set_color("pq_time", self.play_queue_time.to_cursive_color());

        Theme {
            palette,
            shadow: false,
            ..Theme::default()
        }
    }

    /// Get the theme for the custom views of the player UI
    pub fn get_custom_theme(&self) -> HashMap<&'static str, CursiveColor> {
        let mut map = HashMap::new();
        map.insert("bar", self.progress_bar.to_cursive_color());
        map.insert("np_song", self.now_playing_song.to_cursive_color());
        map.insert("np_album", self.now_playing_album.to_cursive_color());
        map.insert("np_artist", self.now_playing_artist.to_cursive_color());
        map.insert("pqp_song", self.play_queue_playing_song.to_cursive_color());
        map.insert("pqp_album", self.play_queue_playing_album.to_cursive_color());
        map.insert("pqp_artist", self.play_queue_playing_artist.to_cursive_color());
        map.insert("pqp_time", self.play_queue_playing_time.to_cursive_color());
        map.insert("pq_song", self.play_queue_song.to_cursive_color());
        map.insert("pq_album", self.play_queue_album.to_cursive_color());
        map.insert("pq_artist", self.play_queue_artist.to_cursive_color());
        map.insert("pq_time", self.play_queue_time.to_cursive_color());
        map
    }

    /// Generate a theme configuration from a list of colors
    ///
    /// Panics if the list of colors is ***not*** 18 elements long
    pub fn from_raw_colors(colors: &[(u8, u8, u8)]) -> ThemeConfig {
        if colors.len() != 18 {
            panic!("The length of the color slice is not correct!")
        }
        ThemeConfig {
            primary: Color::from(colors[0]),
            title_primary: Color::from(colors[1]),
            secondary: Color::from(colors[2]),
            highlight: Color::from(colors[3]),
            highlight_txt: Color::from(colors[4]),
            highlight_inactive: Color::from(colors[5]),

            progress_bar: Color::from(colors[6]),
            now_playing_song: Color::from(colors[7]),
            now_playing_album: Color::from(colors[8]),
            now_playing_artist: Color::from(colors[9]),
            play_queue_playing_song: Color::from(colors[10]),
            play_queue_playing_album: Color::from(colors[11]),
            play_queue_playing_artist: Color::from(colors[12]),
            play_queue_playing_time: Color::from(colors[13]),
            play_queue_song: Color::from(colors[14]),
            play_queue_album: Color::from(colors[15]),
            play_queue_artist: Color::from(colors[16]),
            play_queue_time: Color::from(colors[17])
        }
    }
}

impl Default for ThemeConfig {
    fn default() -> ThemeConfig {
        ThemeConfig {
            primary: Color::Light(BaseColor::White),
            title_primary: Color::Light(BaseColor::Blue),
            secondary: Color::Dark(BaseColor::Blue),
            highlight: Color::Colors256(250),
            highlight_txt: Color::Colors256(233),
            highlight_inactive: Color::Colors256(242),
        
            // theme for the custom views
            progress_bar: Color::Light(BaseColor::Green),
            now_playing_song: Color::Light(BaseColor::Blue),
            now_playing_album: Color::Light(BaseColor::Cyan),
            now_playing_artist: Color::Light(BaseColor::White),
            play_queue_playing_song: Color::Light(BaseColor::Blue),
            play_queue_playing_album: Color::Light(BaseColor::Cyan),
            play_queue_playing_artist: Color::Light(BaseColor::White),
            play_queue_playing_time: Color::Light(BaseColor::Magenta),
            play_queue_song: Color::Dark(BaseColor::Blue),
            play_queue_album: Color::Dark(BaseColor::Cyan),
            play_queue_artist: Color::Dark(BaseColor::White),
            play_queue_time: Color::Dark(BaseColor::Magenta)
        }
    }
}

impl Config for ThemeConfig {
    const FILE_NAME: &'static str = "theme.ron";
    const TYPE: ConfigType = ConfigType::Ron;
    fn get_save_dir() -> String { format!("{}/.config/music-lounge", env::var("HOME").expect("Where the hell is your home folder?!")) }
}

#[derive(Deserialize, Serialize, Copy, Clone)]
enum Color {
    Light(BaseColor),
    Dark(BaseColor),
    Colors256(u8),
    Color24Bit(u8, u8, u8),
    TermDefault
}

impl Color {
    fn to_cursive_color(self) -> CursiveColor{
        match self {
            Color::Light(base) => CursiveColor::Light(base.to_cursive_basecolor()),
            Color::Dark(base) => CursiveColor::Dark(base.to_cursive_basecolor()),
            Color::Colors256(color) => CursiveColor::from_256colors(color),
            Color::Color24Bit(r, g, b) => CursiveColor::Rgb(r, g, b),
            Color::TermDefault => CursiveColor::TerminalDefault
        }
    }

    fn from(color: (u8, u8, u8)) -> Color { Color::Color24Bit(color.0, color.1, color.2) }
}

#[derive(Deserialize, Serialize, Copy, Clone)]
enum BaseColor {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
}

impl BaseColor {
    fn to_cursive_basecolor(self) -> CursiveBaseColor {
        match self {
            BaseColor::Black => CursiveBaseColor::Black,
            BaseColor::Red => CursiveBaseColor::Red,
            BaseColor::Green => CursiveBaseColor::Green,
            BaseColor::Yellow => CursiveBaseColor::Yellow,
            BaseColor::Blue => CursiveBaseColor::Blue,
            BaseColor::Magenta => CursiveBaseColor::Magenta,
            BaseColor::Cyan => CursiveBaseColor::Cyan,
            BaseColor::White => CursiveBaseColor::White
        }
    }
}