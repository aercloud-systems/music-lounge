pub use rust_utils::config::Config;
use rust_utils::config::ConfigType;
use std::env;
use serde_derive::{Deserialize, Serialize};
use crate::PlayerLayout;

#[cfg(feature = "cursive")]
mod theme;

#[cfg(feature = "cursive")]
pub use theme::*;

#[derive(Deserialize, Serialize, Clone)]
/// The configuration for Music Lounge
pub struct MLConfig {
    /// The music library directory
    pub music_dir: String,

    /// Player layout
    #[serde(default = "PlayerLayout::default")]
    pub layout: PlayerLayout,

    /// Should the theme be based on the song's artwork?
    #[serde(default = "def_true")]
    pub dynamic_theme: bool,

    /// Show album thumbnails?
    #[serde(default = "def_true")]
    pub show_album_thumb: bool,

    /// Allow the application to be controlled via MPRIS
    #[serde(default = "def_true")]
    pub use_mpris: bool,

    /// Show/hide wave audio visualizer
    #[serde(default = "def_true")]
    pub show_wave_vis: bool,

    /// Show/hide matrix audio visualizer
    #[serde(default = "def_true")]
    pub show_matrix_vis: bool,

    /// Show/hide track info
    #[serde(default = "def_true")]
    pub show_info: bool,

    /// Placeholder image for track info
    #[serde(default = "placeholder")]
    pub conky_placeholder: String
}

impl Default for MLConfig {
    fn default() -> MLConfig {
        let music_dir = format!("{}/Music", env::var("HOME").expect("Where the hell is your home folder?!"));
        MLConfig {
            music_dir,
            layout: PlayerLayout::Classic,
            dynamic_theme: true,
            show_album_thumb: true,
            use_mpris: true,
            show_wave_vis: true,
            show_matrix_vis: false,
            show_info: true,
            conky_placeholder: placeholder()
        }
    }
}

impl Config for MLConfig {
    const FILE_NAME: &'static str = "config.toml";
    const TYPE: ConfigType = ConfigType::Toml;
    fn get_save_dir() -> String { format!("{}/.config/music-lounge", env::var("HOME").expect("Where the hell is your home folder?!")) }
}

const fn def_true() -> bool { true }
fn placeholder() -> String { "/usr/lib/mlounge/icon.png".to_string() }