#[cfg(feature = "piston")]
use crate::PistonColor;

#[cfg(feature = "cursive")]
use {
    cursive_core::theme::Color,
    crate::config::ThemeConfig
};

#[cfg(any(feature = "cursive", feature = "daemon"))]
use {
    std::{
        ops::RangeInclusive,
        cell::RefCell
    },
    rand::{thread_rng, Rng, prelude::ThreadRng},
};

#[cfg(any(feature = "cursive", feature = "piston"))]
use color_thief::Color as CTColor;

#[cfg(any(feature = "cursive", feature = "daemon", feature = "piston"))]
use {
    std::collections::HashSet,
    color_thief::ColorFormat,
    image::io::Reader as ImageReader
};

#[cfg(any(feature = "cursive", feature = "daemon"))]
thread_local! {
    static RAND: RefCell<ThreadRng> = RefCell::new(thread_rng());
}

pub type RawColor = (u8, u8, u8);

#[cfg(any(feature = "cursive", feature = "daemon"))]
const DEFAULT_CURSIVE_COLORS: [RawColor; 6] = [
    (255, 255, 255), // primary l
    (0, 0, 255),     // title_primary l
    (0, 0, 128),     // secondary d
    (188, 188, 188), // highlight l
    (18, 18, 18),    // highlight_txt d
    (108, 108, 108)  // highlight_inactive l
];

#[cfg(any(feature = "cursive", feature = "daemon"))]
const DEFAULT_COLORS: [RawColor; 12] = [
    (0, 255, 0),     // progress_bar l
    (0, 0, 255),     // now_playing_song l
    (0, 255, 255),   // now_playing_album l
    (255, 255, 255), // now_playing_artist l
    (0, 0, 255),     // play_queue_playing_song l
    (0, 255, 255),   // play_queue_playing_album l
    (255, 255, 255), // play_queue_playing_artist l
    (255, 0, 255),   // play_queue_playing_time l
    (0, 0, 128),     // play_queue_song m
    (0, 255, 255),   // play_queue_album m
    (255, 255, 255), // play_queue_artist m
    (128, 0, 128)    // play_queue_time m
];

pub trait FromRawColor: Copy + Clone {
    fn from_raw(color: RawColor) -> Self;

    #[cfg(any(feature = "cursive", feature = "piston"))]
    fn from_ct_color(color: CTColor) -> Self {
        Self::from_raw((color.r, color.g, color.b))
    }
}

#[cfg(feature = "piston")]
impl FromRawColor for PistonColor {
    fn from_raw(color: RawColor) -> PistonColor { [color.0 as f32 / 255., color.1 as f32 / 255., color.2 as f32 / 255., 1.] }
}

#[cfg(feature = "cursive")]
impl FromRawColor for Color {
    fn from_raw(color: RawColor) -> Color { Color::Rgb(color.0, color.1, color.2) }
}

#[cfg(any(feature = "cursive", feature = "piston"))]
pub fn get_art_colors<C: FromRawColor>(art_path: &str) -> Vec<C> {
    if !art_path.is_empty() {
        let img = ImageReader::open(art_path).unwrap().with_guessed_format().unwrap().decode().unwrap();
        let img_bytes = img.as_bytes().to_vec();
        let mut palette = color_thief::get_palette(&img_bytes, ColorFormat::Rgb, 1, 50).unwrap();
        let mut uniques = HashSet::new();
        palette.retain(|c| uniques.insert(*c));
        return palette.iter()
            .map(|color| C::from_ct_color(*color))
            .collect();
    }

    vec![C::from_raw((90, 174, 222))]
}

// generate a full color theme from the album picture provided
#[cfg(feature = "cursive")]
pub fn get_full_theme(path: &str) -> ThemeConfig {
    let theme_colors = get_album_colors(path);
    ThemeConfig::from_raw_colors(&theme_colors)
}

#[cfg(feature = "daemon")]
pub fn get_list_colors(path: &str) -> (RawColor, RawColor) {
    let theme_colors = get_album_colors(path);
    (theme_colors[1], theme_colors[0])
}

// generate a list of colors based on the picture
#[cfg(any(feature = "cursive", feature = "daemon"))]
fn get_album_colors(path: &str) -> Vec<RawColor> {
    let mut theme_colors = Vec::new();
    RAND.with(|rand_cell| {
        let mut rand = rand_cell.borrow_mut();
        let img = ImageReader::open(path).unwrap().with_guessed_format().unwrap().decode().unwrap();
        let img_bytes = img.as_bytes();
        let raw_palette = color_thief::get_palette(img_bytes, ColorFormat::Rgb, 1, 20).unwrap();
        let mut colors: Vec<RawColor> = Vec::new();
        for color in raw_palette {
            colors.push((color.r, color.g, color.b))
        }
        theme_colors = sort_colors(&mut rand, colors, true);
        let raw_palette2 = color_thief::get_palette(img_bytes, ColorFormat::Rgb, 1, 50).unwrap();
        let mut colors: Vec<RawColor> = Vec::new();
        for color in raw_palette2 {
            colors.push((color.r, color.g, color.b))
        }
        let mut new_colors = sort_colors(&mut rand, colors, false);
        theme_colors.append(&mut new_colors);
    });

    theme_colors
}

// sort colors by brightness and generate the raw color theme
#[cfg(any(feature = "cursive", feature = "daemon"))]
fn sort_colors(rand: &mut ThreadRng, mut colors: Vec<RawColor>, cursive: bool) -> Vec<RawColor> {
    let mut uniques = HashSet::new();
    colors.retain(|c| uniques.insert(*c));
    let mut darks = colors.clone();
    let mut mids = colors.clone();
    let mut lights = colors;
    darks.retain(|c| (30..=85).contains(&get_brightness(*c)));
    mids.retain(|c| (85..=130).contains(&get_brightness(*c)));
    lights.retain(|c| get_brightness(*c) >= 131);
    let mut theme_colors: Vec<RawColor> = Vec::new();
    let default_colors: &[RawColor] = if cursive { &DEFAULT_CURSIVE_COLORS }
    else { &DEFAULT_COLORS };

    for (i, color) in default_colors.iter().enumerate() {
        let ncolor = match i {
            2 | 4 if cursive => get_color(rand, &darks, *color, 0..=50),
            0..=7 if !cursive => get_color(rand, &lights, *color, 100..=255),
            _ => {
                let colors = if cursive { &lights }
                else { &mids };
                get_color(rand, colors, *color, 100..=255)
            }
        };
        theme_colors.push(ncolor);
    }

    theme_colors
}

// get the brightness of a color
#[cfg(any(feature = "cursive", feature = "daemon"))]
fn get_brightness(color: RawColor) -> u8 {
    ((299 * color.0 as u32 + 587 * color.1 as u32 + 114 * color.2 as u32) / 1000) as u8
}

// get a color from a color list, or random grey if it is empty
#[cfg(any(feature = "cursive", feature = "daemon"))]
fn get_color(rand: &mut ThreadRng, colors: &[RawColor], color: RawColor, range: RangeInclusive<u8>) -> RawColor {
    if colors.is_empty() {
        let a = rand.gen_range(range);
        (a, a, a)
    }
    else {
        *colors.iter().min_by_key(|&acolor| sub_colors(*acolor, color)).unwrap()
    }
}

// subtract 2 colors from each other
#[cfg(any(feature = "cursive", feature = "daemon"))]
fn sub_colors(color_l: RawColor, color_r: RawColor) -> RawColor {
    (
        (color_l.0 as i16 - color_r.0 as i16).unsigned_abs() as u8,
        (color_l.1 as i16 - color_r.1 as i16).unsigned_abs() as u8,
        (color_l.2 as i16 - color_r.2 as i16).unsigned_abs() as u8
    )
}