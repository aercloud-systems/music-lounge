//! # Music Lounge core library
//!
//! Core logic for Music Lounge
//!
//! ## Features
//! * `piston`: piston graphics engine support
//! * `cursive`: cursive TUI library support
//! * `client`: client logic
use serde_derive::{Deserialize, Serialize};
use lazy_static::lazy_static;
use std::env;
pub mod config;
pub mod library;
pub(crate) mod album_colors;

pub mod player;

#[cfg(feature = "piston")]
type PistonColor = [f32; 4];

lazy_static!{
    static ref TMP_DIR: String =  format!("/tmp/mlounge-{}", env::var("USER").expect("What is your name again?"));
}

/// Layout of the Music Lounge UI
#[derive(Copy, Clone, Deserialize, Serialize, Eq, PartialEq, Default)]
pub enum PlayerLayout {
    Cupertino,
    Metropolis,
    #[default]
    Classic,
}