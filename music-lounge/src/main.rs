use std::{
    env, thread,
    time::Duration
};
use lazy_static::lazy_static;
use rust_utils::utils;
use sysinfo::System;
use mlounge_core::config::{MLConfig, Config};

#[cfg(debug_assertions)]
use std::fs;

mod tui;

lazy_static! {
    static ref CONFIG: MLConfig = MLConfig::load();
}

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    // launch the daemon if it isn't already
    #[cfg(not(debug_assertions))]
    let mlounged_prog = "/usr/bin/mlounged";

    #[cfg(debug_assertions)]
    let mlounged_prog = if fs::read("mlounged").is_err() {
        "target/debug/mlounged"
    }
    else { "./mlounged" };

    let mut system = System::new();
    system.refresh_all();
    let mut launch_daemon = true;
    let mut pid = 0;
    for (id, proc) in system.processes() {
        if let Some(exe) = proc.exe() {
            let exe_str = exe.display().to_string();
            if exe_str.contains("mlounged") {
                pid = id.as_u32();
                launch_daemon = false;
                break;
            }
        }
    }

    if launch_daemon {
        let proc = utils::spawn_process(mlounged_prog, false, false, [""]);
        pid = proc.id();
        thread::sleep(Duration::from_secs(2));
    }
    println!("Player process ID: {pid}");

    // let's reload the music library if `-r` is passed
    let mut args = env::args();

    if let Some(string) = args.nth(1) {
        if string == "-r" {
            mlounge_client::rescan_library().unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    }

    tui::show_ui();
}