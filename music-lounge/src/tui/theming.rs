use super::{
    views::{PlaybackProgress, PlayerStatusView},
    layouts::PlayQueueList
};
use crate::CONFIG;
use cursive::{
    Cursive,
    utils::markup::StyledString,
    view::SizeConstraint,
    views::{
        ResizedView,
        TextView,
        ViewRef
    }
};
use cursive_extras::*;
use mlounge_client::MLoungeClient;
use mlounge_core::{
    config::{ThemeConfig, Config},
    PlayerLayout
};

// reset the theme to the one in the config file
pub fn reset_theme(root: &mut Cursive) {
    let mut album_art: ViewRef<ImageView> = root.find_name("album_art").unwrap();
    *album_art = if CONFIG.layout == PlayerLayout::Classic {
        ImageView::new(6, 3)
    }
    else {
        ImageView::new(8, 4)
    }
        .minimize(true);

    let old_theme = ThemeConfig::load().to_cursive_theme();
    root.set_theme(old_theme);
    let mut queue = root.find_name::<PlayQueueList>("play_queue").unwrap();
    queue.clear();
}

// set the theme of the application with the album art color and update album art tile
pub fn update_theme(root: &mut Cursive, refresh: bool) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    let queue = player.queue.clone();
    let now_playing = player.song.clone();

    if let Some(path) = player.song.get_art_path() {
        if CONFIG.dynamic_theme {
            let song_theme = player.song.tui_theme();
            root.set_theme(song_theme);
        }
        if CONFIG.show_album_thumb {
            let mut album_art: ViewRef<ImageView> = root.find_name("album_art").unwrap();
            album_art.set_image(&path);
        }
    }
    else { reset_theme(root); }

    let custom_colors = root.current_theme().palette.clone();
    let mut artist_label: ViewRef<ResizedView<TextView>> = root.find_name("artist_l").unwrap();
    let mut album_label: ViewRef<ResizedView<TextView>> = root.find_name("album_l").unwrap();
    let mut song_label: ViewRef<ResizedView<TextView>> = root.find_name("song_l").unwrap();

    // the width of the play queue
    let mut subwidth = 0;
    if let Some(pq_div) = root.find_name::<VDivider>("pq_div") {
        let width = pq_div.width();
        if width > 7 {
            subwidth = (width - 7) / 3;
        }
    }

    artist_label.set_width(SizeConstraint::Fixed(subwidth));
    album_label.set_width(SizeConstraint::Fixed(subwidth));
    song_label.set_width(SizeConstraint::Fixed(subwidth));

    // update play queue view
    if let Some(mut play_queue) = root.find_name::<PlayQueueList>("play_queue") {
        if refresh {
            // prevent the selected item from being reset on update
            let selected = play_queue.selected_index();
            play_queue.clear();
            for song in queue.iter() {
                let name = format!("{: <subwidth$} ", song.name);
                let album_str = format!("{: <subwidth$} ", song.album);
                let artist_str = format!("{: <subwidth$} ", song.artist);
                let duration = song.get_duration_str();
                let mut styled = StyledString::default();
                if *song == now_playing {
                    styled.append_styled(&artist_str[0..subwidth - 1], *custom_colors.custom("pqp_artist").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pqp_artist").unwrap());
                    styled.append_styled(&name[0..subwidth - 1], *custom_colors.custom("pqp_song").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pqp_song").unwrap());
                    styled.append_styled(&album_str[0..subwidth - 1], *custom_colors.custom("pqp_album").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pqp_album").unwrap());
                    styled.append_styled(&duration, *custom_colors.custom("pqp_time").unwrap());
                }
                else {
                    styled.append_styled(&artist_str[0..subwidth - 1], *custom_colors.custom("pq_artist").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pq_artist").unwrap());
                    styled.append_styled(&name[0..subwidth - 1], *custom_colors.custom("pq_song").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pq_song").unwrap());
                    styled.append_styled(&album_str[0..subwidth - 1], *custom_colors.custom("pq_album").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pq_album").unwrap());
                    styled.append_styled(&duration, *custom_colors.custom("pq_time").unwrap());
                }

                play_queue.add_item(styled, song.clone());
            }

            // restore the user's chosen item (if there was one)
            play_queue.set_selection(selected);
        }
        else {
            for (label, song) in play_queue.iter_mut() {
                let name = format!("{: <subwidth$} ", song.name);
                let album_str = format!("{: <subwidth$} ", song.album);
                let artist_str = format!("{: <subwidth$} ", song.artist);
                let duration = song.get_duration_str();
                let mut styled = StyledString::new();
                if *song == now_playing {
                    styled.append_styled(&artist_str[0..subwidth - 1], *custom_colors.custom("pqp_artist").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pqp_artist").unwrap());
                    styled.append_styled(&name[0..subwidth - 1], *custom_colors.custom("pqp_song").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pqp_song").unwrap());
                    styled.append_styled(&album_str[0..subwidth - 1], *custom_colors.custom("pqp_album").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pqp_album").unwrap());
                    styled.append_styled(&duration, *custom_colors.custom("pqp_time").unwrap());
                }
                else {
                    styled.append_styled(&artist_str[0..subwidth - 1], *custom_colors.custom("pq_artist").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pq_artist").unwrap());
                    styled.append_styled(&name[0..subwidth - 1], *custom_colors.custom("pq_song").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pq_song").unwrap());
                    styled.append_styled(&album_str[0..subwidth - 1], *custom_colors.custom("pq_album").unwrap());
                    styled.append_styled(" ", *custom_colors.custom("pq_album").unwrap());
                    styled.append_styled(&duration, *custom_colors.custom("pq_time").unwrap());
                }

                *label = styled;
            }
        }

        // update the theme of the progress bar and status
        let mut player_prog: ViewRef<PlaybackProgress> = root.find_name("progress").unwrap();
        let mut status: ViewRef<PlayerStatusView> = root.find_name("player_status").unwrap();

        status.set_colors([
            *custom_colors.custom("np_song").unwrap(),
            *custom_colors.custom("np_album").unwrap(),
            *custom_colors.custom("np_artist").unwrap()
        ]);
        status.set_song(now_playing);
        player_prog.set_color(*custom_colors.custom("bar").unwrap());
        drop(player_prog);
        drop(status);
    }
}