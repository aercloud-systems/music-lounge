use crate::CONFIG;
use super::{
    PlayerLayout,
    layouts::{self, new_lib_loading_anim},
    theming::update_theme
};
use std::rc::Rc;
use cursive::{
    Cursive, With,
    event::{Key, Event},
    theme::ColorStyle,
    view::{Nameable, Scrollable},
    utils::markup::StyledString,
    traits::Resizable,
    views::{
        LinearLayout,
        ResizedView,
        Dialog,
        SelectView,
        TextView,
        EditView,
        OnEventView,
        ViewRef
    }
};
use cursive_async_view::AsyncView;
use cursive_extras::*;
use mlounge_client::{MLoungeClient, RepeatMode};
use mlounge_core::library::{
    Playlist,
    PlaylistAddable,
    PlaylistStore,
    Song
};

pub fn playlist_editor(root: &mut Cursive) {
    let pl_view = AsyncView::new_with_bg_creator(root, mlounge_client::get_playlists, gen_playlist_view)
        .with_animation_fn(new_lib_loading_anim("Loading Playlists..."));

    root.add_layer(
        c_focus!(
            Dialog::around(pl_view)
                .title("Playlists")
                .button("New Playlist", |root| {
                    let mut pl_name_ev = styled_editview("", "pname", false);
                    pl_name_ev.get_mut().set_on_submit(|root, name| {
                        let mut list_view: ViewRef<SelectView<Playlist>> = root.find_name("playlists").unwrap();
                        if let Ok(mut lists) = mlounge_client::get_playlists() {
                            let new_playlist = Playlist::new(name);
                            lists.add_playlist(new_playlist.clone());
                            list_view.add_item(&new_playlist.name, new_playlist.clone());
                            root.pop_layer();
                        }
                    });

                    root.add_layer(
                        Dialog::around(
                            hlayout!(
                                TextView::new("Playlist Name: "),
                                ResizedView::with_min_width(30, pl_name_ev)
                            )
                        )
                            .title("New Playlist")
                            .dismiss_button("Back")
                            .button("Submit", |root| {
                                let pname: ViewRef<EditView> = root.find_name("pname").unwrap();
                                let mut list_view: ViewRef<SelectView<Playlist>> = root.find_name("playlists").unwrap();
                                if let Ok(mut lists) = mlounge_client::get_playlists() {
                                    let new_playlist = Playlist::new(&pname.get_content());
                                    lists.add_playlist(new_playlist.clone());
                                    list_view.add_item(&new_playlist.name, new_playlist.clone());
                                    root.pop_layer();
                                }
                            })
                    );
                })
                .dismiss_button("Back")
                .wrap_with(OnEventView::new)
                .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
        )
    );
}

//TODO: option to add songs to a new playlist
pub fn add_to_playlist<I: PlaylistAddable + 'static>(root: &mut Cursive, items: I) {
    if let Ok(lists) = mlounge_client::get_playlists() {
        let mut list_view: SelectView<Playlist> = SelectView::new()
            .on_submit(move |root, playlist: &Playlist| {
                if let Ok(mut lists) = mlounge_client::get_playlists() {
                    for list in lists.iter_mut() {
                        if list == playlist {
                            list.add_items(&items);
                            root.pop_layer();
                        }
                    }

                    lists.update().unwrap();
                }
            });

        for list in lists.iter() {
            list_view.add_item(&list.name, list.clone());
        }

        root.add_layer(
            Dialog::around(list_view)
                .title("Add to Playlist")
                .dismiss_button("Back")
                .wrap_with(OnEventView::new)
                .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
        );
    }
}

pub fn gen_playlist_view(lists: PlaylistStore) -> LinearLayout {
    let layout = CONFIG.layout;
    let mut list_view: SelectView<Playlist> = SelectView::new()
        .on_submit(|root, list: &Playlist| {
            if !list.songs.is_empty() {
                process_playlist(root, list, &list.songs[0], false);
            }
        })
        .on_select(|root, list: &Playlist| {
            let mut songs: ViewRef<AdvancedSelectView<Song>> = root.find_name("songs").expect("I swear this is the correct view!");
            let mut num_songs: ViewRef<TextView> = root.find_name("num_songs").expect("I swear this is the correct view!");
            songs.clear();
            num_songs.set_content(format!("{} songs", list.songs.len()));
            if !list.songs.is_empty() {
                for song in list.iter() {
                    let mut song_label = StyledString::styled(&song.artist, ColorStyle::title_primary());
                    song_label.append(" ");
                    song_label.append(&song.name);
                    songs.add_item(song_label, song.clone());
                }
            }
        })
        .with_all(
            lists.iter()
                .map(|list| (&list.name, list.clone()))
        );

    if layout != PlayerLayout::Classic {
        list_view.set_on_select(|root, list: &Playlist| {
            let mut songs: ViewRef<AdvancedSelectView<Song>> = root.find_name("songs").expect("I swear this is the correct view!");
            let mut num_songs: ViewRef<TextView> = root.find_name("num_songs").expect("I swear this is the correct view!");
            let mut list_title: ViewRef<TextView> = root.find_name("list_title").expect("I swear this is the correct view!");
            let mut thumb: ViewRef<LinearLayout> = root.find_name("thumb").expect("I swear this is the correct view!");
            songs.clear();
            list_title.set_content(&list.name);
            *thumb = gen_playlist_thumb(list);
            if list.songs.is_empty() {
                return;
            }

            for song in list.iter() {
                let (artist_color, name_color) = song.list_colors();
                let mut song_label = StyledString::styled(&song.artist, artist_color);
                song_label.append(" ");
                song_label.append_styled(&song.name, name_color);
                songs.add_item(song_label, song.clone());
            }

            num_songs.set_content(format!("{} songs", list.songs.len()));
        })
    }
    let mut song_list = AdvancedSelectView::new()
        .on_submit(|root, song: &Song| {
            let list_view: ViewRef<SelectView<Playlist>> = root.find_name("playlists").unwrap();
            let playlist = &*list_view.selection().unwrap();
            process_playlist(root, playlist, song, false);
        });

    let mut num_song_view = TextView::new("0 songs");
    let mut list_layout = LinearLayout::vertical();
    if !list_view.is_empty() {
        let first_list = list_view.get_item(0).unwrap().1;
        for song in &first_list.songs {
            if layout != PlayerLayout::Classic {
                let (artist_color, name_color) = song.list_colors();
                let mut song_label = StyledString::styled(&song.artist, artist_color);
                song_label.append(" ");
                song_label.append_styled(&song.name, name_color);
                song_list.add_item(song_label, song.clone());
            }
            else {
                let mut song_label = StyledString::styled(&song.artist, ColorStyle::title_primary());
                song_label.append(" ");
                song_label.append(&song.name);
                song_list.add_item(song_label, song.clone());
            }
        }
        num_song_view.set_content(format!("{} songs", first_list.songs.len()));
    }

    let song_list_ev = song_list
        .with_name("songs")
        .scrollable()
        .wrap_with(OnEventView::new)
        .on_event(Event::Key(Key::Del), move |root| {
            let mut playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
            let playlist = playlists.selection().unwrap();
            let mut songs: ViewRef<AdvancedSelectView<Song>> = root.find_name("songs").expect("I swear this is the correct view!");
            let selected = songs.selection().unwrap();
            let idx = songs.selected_index();
            let pl_idx = playlists.selected_id().unwrap();
            if let Ok(mut lists) = mlounge_client::get_playlists() {
                let mut rm_list = false;
                for list in lists.iter_mut() {
                    if *list == *playlist {
                        rm_list = true;
                        list.remove_song(selected);
                    }
                }
                if rm_list { songs.remove_item(idx); }
                lists.update().unwrap();
                playlists.clear();
                for list in lists.iter() {
                    playlists.add_item(&list.name, list.clone());
                }
                playlists.set_selection(pl_idx);
            }

        })
        .on_event(Event::Key(Key::F5), |root| move_item(root, true))
        .on_event(Event::Key(Key::F6), |root| move_item(root, false))
        .on_event('z', |root| {
            let playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
            let songs: ViewRef<AdvancedSelectView<Song>> = root.find_name("songs").expect("I swear this is the correct view!");
            if let Some(playlist) = playlists.selection() {
                if let Some(song) = songs.selection() {
                    process_playlist(root, &playlist, song, true);
                }
                else if let Some(song) = playlist.songs.get(0) {
                    process_playlist(root, &playlist, song, true);
                }
            }
        });

    if layout == PlayerLayout::Classic || list_view.is_empty() {
        list_layout.add_child(song_list_ev);
        list_layout.add_child(fixed_vspacer(1));
        list_layout.add_child(num_song_view.with_name("num_songs"));
    }
    else {
        let first_list = list_view.get_item(0).unwrap().1;
        let thumb = gen_playlist_thumb(first_list);
        list_layout.add_child(
            hlayout!(
                thumb.with_name("thumb"),
                fixed_hspacer(1),
                vlayout!(
                    TextView::new(&first_list.name).with_name("list_title"),
                    num_song_view.with_name("num_songs")
                )
            )
        );
        list_layout.add_child(fixed_vspacer(1));
        list_layout.add_child(song_list_ev);
    }

    hlayout!(
        list_view
            .with_name("playlists")
            .scrollable()
            .wrap_with(OnEventView::new)
            // delete playlist
            .on_event(Event::Key(Key::Del), |root| {
                root.add_layer(confirm_dialog("Delete Playlist", "Are you sure?", move |root| {
                    let mut playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
                    let name = &playlists.selection().unwrap().name;
                    if let Ok(mut lists) = mlounge_client::get_playlists() {
                        lists.remove_playlist(name);
                        playlists.clear();
                        for list in lists.iter() {
                            playlists.add_item(&list.name, list.clone());
                        }
                        lists.update().unwrap();
                        root.pop_layer();
                    }
                }));
            })
            // add playlist to play queue
            .on_event('a', |root| {
                let playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
                let playlist = playlists.selection().unwrap();
                layouts::play_or_add_to_queue(root, &*playlist);
            })
            // shuffle play playlist
            .on_event('z', |root| {
                let playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
                if let Some(playlist) = playlists.selection() {
                    if let Some(song) = playlist.songs.get(0) {
                        process_playlist(root, &playlist, song, true);
                    }
                }
            })
            // rename playlist
            .on_event(Event::Key(Key::F2), |root| {
                let playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
                let old_name = Rc::new(playlists.selection().unwrap().name.to_string());
                drop(playlists);
                let old_name2 = old_name.clone();
                let old_name3 = old_name.clone();
                let mut renamer = styled_editview(&old_name, "renamer", false);
                renamer.get_mut().set_on_submit(move |root, new_name| {
                    if let Ok(mut lists) = mlounge_client::get_playlists() {
                        lists.rename_playlist(old_name2.as_str(), new_name);
                        let mut playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
                        playlists.clear();
                        for list in lists.iter() {
                            playlists.add_item(&list.name, list.clone());
                        }
                        lists.update().unwrap();
                        root.pop_layer();
                        drop(playlists);
                    }
                });

                root.add_layer(
                    Dialog::around(
                        hlayout!(TextView::new("New Name: "), renamer.fixed_width(40))
                    )
                        .title("Rename Playlist")
                        .button("Save", move |root| {
                            let mut playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").expect("I swear this is the correct view!");
                            let renamer: ViewRef<EditView> = root.find_name("renamer").unwrap();
                            if let Ok(mut lists) = mlounge_client::get_playlists() {
                                lists.rename_playlist(old_name3.as_str(), renamer.get_content().as_str());
                                playlists.clear();
                                for list in lists.iter() {
                                    playlists.add_item(&list.name, list.clone());
                                }
                                lists.update().unwrap();
                                root.pop_layer();
                                drop(playlists);
                            }
                        })
                        .dismiss_button("Back")
                        .wrap_with(OnEventView::new)
                        .on_event(Event::Key(Key::Esc), |root| { root.pop_layer(); })
                );
            }),

        HDivider::new(),
        list_layout.with_name("list_layout")
    )
}

// move selected item up or down in a playlist
fn move_item(root: &mut Cursive, up: bool) {
    let mut playlists: ViewRef<SelectView<Playlist>> = root.find_name("playlists").unwrap();
    let playlist = playlists.selection().unwrap();
    let mut songs: ViewRef<AdvancedSelectView<Song>> = root.find_name("songs").unwrap();
    let idx = songs.selected_index();
    let pl_idx = playlists.selected_id().unwrap();

    if let Ok(mut lists) = mlounge_client::get_playlists() {
        for list in lists.iter_mut() {
            if *list == *playlist {
                list.move_item(idx, up)
            }
        }

        lists.update().unwrap();
        playlists.clear();
        for list in lists.iter() {
            playlists.add_item(&list.name, list.clone());
        }
        playlists.set_selection(pl_idx);

        if up {
            songs.move_item_up(idx);
            songs.set_selection(idx - 1);
        }
        else {
            songs.move_item_down(idx);
            songs.set_selection(idx + 1);
        }
    }
}

// generate a playlist thumbnail
fn gen_playlist_thumb(playlist: &Playlist) -> LinearLayout {
    if let Some(images) = playlist.get_disp_images() {
        vlayout!(
            hlayout!(
                ImageView::new(8, 4).image(&images[0]),
                ImageView::new(8, 4).image(&images[1])
            ),
            hlayout!(
                ImageView::new(8, 4).image(&images[2]),
                ImageView::new(8, 4).image(&images[3])
            )
        )
    }
    else { hlayout!() }
}

// process a selected playlist
fn process_playlist(root: &mut Cursive, playlist: &Playlist, first_song: &Song, shuffle: bool) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.load_list(playlist, first_song).unwrap();
    let new_theme = first_song.tui_theme();

    if shuffle {
        player.shuffle_queue().unwrap();
        while player.repeat != RepeatMode::All {
            player.cycle_repeat().unwrap();
        }
        player.sync(false).unwrap();
    }
    if CONFIG.dynamic_theme {
        root.set_theme(new_theme);
    }

    if let Some(path) = first_song.get_art_path() {
        if CONFIG.show_album_thumb {
            let mut album_art: ViewRef<ImageView> = root.find_name("album_art").unwrap();
            album_art.set_image(&path);
        }
    }
    else { super::reset_theme(root); }
    update_theme(root, true);
    root.pop_layer();
}