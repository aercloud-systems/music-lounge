use super::{
    theming::update_theme,
    layouts::{PlayQueueList, PlayQueueScroller}
};
use crate::{CONFIG, VERSION};
use std::{
    thread,
    time::Duration
};
use cursive::{
    Cursive, With, view::Nameable,
    event::{Key, Event},
    views::{
        Dialog,
        TextView,
        BoxedView,
        OnEventView,
        SelectView,
        Checkbox,
        EditView,
        NamedView,
        ViewRef, Button
    },
    traits::Resizable
};
use cursive_audiovis::{MatrixVisualizer, WaveVisualizer};
use cursive_extras::*;
use mlounge_client::MLoungeClient;
use mlounge_core::{
    PlayerLayout,
    config::{Config, MLConfig}
};
use rust_utils::utils;

#[cfg(debug_assertions)]
use std::fs;

// song art viewer dialog
pub fn view_song_art(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    if let Some(path) = player.song.get_art_path() {
        let mut art_view = ImageView::new(80, 40);
        art_view.set_image(&path);
        root.add_layer(
            Dialog::around(art_view)
                .title("Song Artwork")
                .dismiss_button("Back")
                .wrap_with(OnEventView::new)
                .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
        );
    }
}

// help and about dialog
pub fn help_dialog(root: &mut Cursive) {
    #[cfg(debug_assertions)]
    let icon = if fs::read("/usr/share/mlounge/icon.png").is_err() { "icon.png" }
    else { "/usr/share/mlounge/icon.png" };

    #[cfg(not(debug_assertions))]
    let icon = "/usr/share/mlounge/icon.png";

    let icon_view = ImageView::new(10, 5)
        .image(icon);

    let help_text = format!(
        "Music Lounge\n\
        Version {VERSION}\n\n\
        Controls:\n\n\
        Main UI:\n\
        s: Stop player\n\
        a: Add selected item to a playlist\n\
        n: Next track\n\
        p: Previous track\n\
        q: Quit\n\
        Shift + r: Restart current track\n\
        Shift + a: Add play queue to a playlist\n\
        [Space]: Play/pause\n\
        z: Shuffle play queue\n\
        r: Set repeat mode\n\
        F5: Move play queue item up\n\
        F6: Move play queue item down\n\
        Del: Remove play queue item\n\
        Ctrl + f: Play queue song finder\n\n\
        Playlists:\n\
        Del: Delete playlist or playlist item\n\
        F2: Rename playlist\n\
        F5: Move playlist item up\n\
        F6: Move playlist item down\n\
        a: Add playlist to play queue\n\
        z: Shuffle play playlist\n\n\
        Run mlounge -r to rescan the music library"
    );
    
    root.add_layer(
        Dialog::around(
            hlayout!(
                TextView::new(help_text),
                icon_view
            )
        )
            .title("Help")
            .dismiss_button("Back")
            .wrap_with(OnEventView::new)
            .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
    );
}

// next song
pub fn player_next(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.next_song().unwrap();
    update_theme(root, false);
}

// previous song
pub fn player_prev(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.prev_song().unwrap();
    update_theme(root, false);
}

// stop the player
pub fn player_stop(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.stop().unwrap();
    super::reset_theme(root);
}

// rewind the current song
pub fn player_restart(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.restart().unwrap();
}

// pause/resume player
pub fn toggle_playback(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.play_pause().unwrap();
}

// shuffle the play queue
pub fn queue_shuffle(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.shuffle_queue().unwrap();
    let mut play_queue_scroll = root.find_name::<PlayQueueScroller>("play_queue_scroll").unwrap();
    play_queue_scroll.scroll_to_top();
    play_queue_scroll.get_inner_mut().get_mut().set_selection(0);
    drop(play_queue_scroll);
    update_theme(root, true)
}

// cycle the repeat mode
pub fn cycle_repeat(root: &mut Cursive) {
    let player = root.user_data::<MLoungeClient>().unwrap();
    player.cycle_repeat().unwrap();
}

// move play queue up or down in the list
pub fn move_queue_item(root: &mut Cursive, up: bool) {
    let mut songs: ViewRef<PlayQueueList> = root.find_name("play_queue").unwrap();
    let idx = songs.selected_index();
    let player = root.user_data::<MLoungeClient>().unwrap();
    
    player.queue.move_item(idx, up);

    if up {
        songs.move_item_up(idx);
        songs.set_selection(idx - 1);
    }
    else {
        songs.move_item_down(idx);
        songs.set_selection(idx + 1);
    }

    player.sync(false).unwrap();
}

// settings dialog
pub fn settings_dialog(root: &mut Cursive) {
    let layouts = select_view! {
        "Cupertino" => PlayerLayout::Cupertino,
        "Metropolis" => PlayerLayout::Metropolis,
        "Classic" => PlayerLayout::Classic
    }
        .selected(CONFIG.layout as usize)
        .popup();

    root.add_layer(
        settings!(
            "Settings",
            |view| {
                let music_dir: ViewRef<EditView> = view.find_name("music_dir").unwrap();
                let placeholder: ViewRef<EditView> = view.find_name("placeholder").unwrap();
                let layouts: ViewRef<SelectView<PlayerLayout>> = view.find_name("layouts").unwrap();
                let dyntheme: ViewRef<Checkbox> = view.find_name("dyntheme").unwrap();
                let album_thumb: ViewRef<Checkbox> = view.find_name("album_thumb").unwrap();
                let mut config = MLConfig::load();
                config.music_dir = music_dir.get_content().to_string();
                config.layout = *(layouts.selection().unwrap());
                config.dynamic_theme = dyntheme.is_checked();
                config.show_album_thumb = album_thumb.is_checked();
                config.use_mpris = get_checkbox_option(view, "use_mpris");
                config.show_wave_vis = get_checkbox_option(view, "show_wave_vis");
                config.show_matrix_vis = get_checkbox_option(view, "show_matrix_vis");
                config.show_info = get_checkbox_option(view, "show_info");
                config.conky_placeholder = placeholder.get_content().to_string();
                config.save().expect("Unable to save settings!");
                view.pop_layer();
            },
            TextView::new("App settings:"),
            hlayout!(
                TextView::new("Player Layout: "),
                layouts.with_name("layouts")
            ),
            labeled_checkbox("Album art dynamic theme", "dyntheme", CONFIG.dynamic_theme),
            labeled_checkbox("Album art thumbnails", "album_thumb", CONFIG.show_album_thumb),
            fixed_vspacer(1),
            TextView::new("Background process settings:"),
            labeled_checkbox("Remote Control", "use_mpris", CONFIG.use_mpris),
            labeled_checkbox("Show Wave Visualizer", "show_wave_vis", CONFIG.show_wave_vis),
            labeled_checkbox("Show Matrix Visualizer", "show_matrix_vis", CONFIG.show_matrix_vis),
            labeled_checkbox("Show player status on desktop", "show_info", CONFIG.show_info),
            fixed_vspacer(1),
            TextView::new("Info placeholder image:"),
            styled_editview(&CONFIG.conky_placeholder, "placeholder", false),
            fixed_vspacer(1),
            TextView::new("Music library directory:"),
            styled_editview(&CONFIG.music_dir, "music_dir", false),
            fixed_vspacer(1),
            Button::new("Rescan Library", |root| {
                root.add_layer(
                    confirm_dialog("Are you sure?", "The music library will be rescanned and the app will exit.", 
                        |root|{
                            mlounge_client::rescan_library().unwrap();
                            root.quit();
                        }
                    )
                );
            }),
            Button::new("Restart background process", |root| {
                root.add_layer(
                    confirm_dialog("Are you sure?", "The background process will be restarted and the app will exit.", 
                        |root|{
                            utils::run_command("killall", false, ["-2", "mlounged"]);
                            thread::sleep(Duration::from_secs(2));

                            #[cfg(not(debug_assertions))]
                            let mlounged_prog = "/usr/bin/mlounged";

                            #[cfg(debug_assertions)]
                            let mlounged_prog = if fs::read("mlounged").is_err() {
                                "target/debug/mlounged"
                            }
                            else { "./mlounged" };

                            utils::spawn_process(mlounged_prog, false, false, [""]);
                            root.quit();
                        }
                    )
                );
            }),
            fixed_vspacer(1),
            TextView::new("Music Lounge will need to be restarted\nfor changes to take effect")
        )
    );
}

// audio visualizer view
pub fn visualizer(root: &mut Cursive) {
    if root.find_name::<WaveVisualizer>("visual").is_some() || root.find_name::<MatrixVisualizer>("visual").is_some() { return; }
    root.add_fullscreen_layer(
        BoxedView::boxed(WaveVisualizer::new().with_name("visual"))
            .with_name("boxed_vis")
            .full_screen()
            .wrap_with(OnEventView::new)
            .on_event(Key::Esc, |view| { view.pop_layer(); })
            .on_event('v', |view| {
                let mut boxed_vis = view.find_name::<BoxedView>("boxed_vis").unwrap();
                if boxed_vis.is::<NamedView<MatrixVisualizer>>() {
                    *boxed_vis = BoxedView::boxed(WaveVisualizer::new().with_name("visual"));
                }
                else if boxed_vis.is::<NamedView<WaveVisualizer>>() {
                    *boxed_vis = BoxedView::boxed(MatrixVisualizer::new().with_name("visual"));
                }
            })
    )
}