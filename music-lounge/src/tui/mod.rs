use cursive::{
    With, Cursive, View,
    event::{Key, Event},
    views::{
        LinearLayout,
        OnEventView,
        HideableView,
        ViewRef,
        ResizedView, Button
    },
    view::{Selector, Finder},
    traits::{Nameable, Scrollable}
};
use cursive_async_view::AsyncView;
use cursive_extras::{
    AdvancedSelectView,
    VDivider,
    LazyView,
    vlayout,
    hlayout,
    filled_hspacer,
    fixed_hspacer,
    buffered_backend_root
};
use cursive_audiovis::{MatrixVisualizer, WaveVisualizer};
use mlounge_client::{MLoungeClient, PlayerState};
use mlounge_core::{
    PlayerLayout,
    config::{ThemeConfig, Config}
};
use crate::CONFIG;

mod events;
mod theming;
mod views;
mod playlist_editor;
mod layouts;

use self::{
    views::{PlaybackProgress, PlayerStatusView},
    playlist_editor::*,
    theming::*,
    events::*,
    layouts::*
};

type LibraryView = AsyncView<ResizedView<LinearLayout>>;

pub fn show_ui() {
    let mut root = buffered_backend_root();
    let play_queue_ev = AdvancedSelectView::new()
        .on_submit(|root, song| {
            let player = root.user_data::<MLoungeClient>().unwrap();
            player.set_pos(song).unwrap();
            update_theme(root, false);
        })
        .with_name("play_queue")
        .scrollable()
        .with_name("play_queue_scroll")
        .wrap_with(OnEventView::new)
        .on_event(Event::Key(Key::F5), |root| move_queue_item(root, true))
        .on_event(Event::Key(Key::F6), |root| move_queue_item(root, false))
        .on_event(Event::Key(Key::Del), |root| {
            let mut songs: ViewRef<PlayQueueList> = root.find_name("play_queue").unwrap();
            let selected = songs.selection().unwrap().clone();
            let player = root.user_data::<MLoungeClient>().unwrap();
            if selected != player.song {
                let idx = songs.selected_index();
                player.queue.remove_song(&selected);
                songs.remove_item(idx);
                player.sync(false).unwrap();
            }
        })
        .on_event('a', |root| {
            let selector: ViewRef<PlayQueueList> = root.find_name("play_queue").unwrap();
            if let Some(selected) = selector.selection() {
                add_to_playlist(root, selected.clone());
            };
        })
        .on_event('A', |root| {
            let player = root.user_data::<MLoungeClient>().unwrap();
            let songs = player.queue.songs.clone();
            add_to_playlist(root, songs);
        });

    let layout = get_layout(CONFIG.layout, play_queue_ev, &mut root);
    root.set_theme(ThemeConfig::load().to_cursive_theme());
    root.set_user_data(MLoungeClient::new());

    root.add_fullscreen_layer(
        vlayout!(
            hlayout!(
                fixed_hspacer(1),
                Button::new_raw(" Playlists ", playlist_editor),
                Button::new_raw(" Artwork ", view_song_art),
                Button::new_raw(" Settings ", settings_dialog),
                Button::new_raw(" Help ", help_dialog),
                Button::new_raw(" Visualizer ", visualizer),
                filled_hspacer()
            ),
            layout
        )
    );

    let mut startup = true;
    let mut queue_width = 0usize;

    // refresh event
    root.add_global_callback(Event::Refresh, move |root| {
        let player = root.user_data::<MLoungeClient>().unwrap();
        let song_changed = player.song_changed();
        player.sync(true).unwrap();
        let song = player.song.clone();
        let cur_time = player.cur_time();
        let secs = player.cur_time_secs();
        let repeat = player.repeat;
        let state = player.state;
        let stopped = player.state == PlayerState::Stopped;

        if let Some(pq_div) = root.find_name::<VDivider>("pq_div") {
            if !startup {
                let cur_width = pq_div.width();
                if queue_width != cur_width {
                    drop(pq_div);
                    update_theme(root, true);
                    queue_width = cur_width;
                }
            }
        }

        // keep the player's timing in sync with the playing audio and change the theme if needed
        if song_changed || startup {
            update_theme(root, startup);
            startup = false;
        };

        // update the visualizers if they are visible
        if let Some(mut visualizer) = root.find_name::<MatrixVisualizer>("visual") {
            visualizer.update(&song);
            return;
        }

        if let Some(mut visualizer) = root.find_name::<WaveVisualizer>("visual") {
            visualizer.update(&song);
            return;
        }

        // views to refresh
        let mut player_prog: ViewRef<PlaybackProgress> = root.find_name("progress").unwrap();
        let mut status: ViewRef<PlayerStatusView> = root.find_name("player_status").unwrap();
        let mut libview: ViewRef<HideableView<LibraryView>> = root.find_name("libview").unwrap();

        // if this is the cupertino or metropolis layout,
        // asynchronusly load the album view to avoid UI lag
        if matches!(CONFIG.layout, PlayerLayout::Metropolis | PlayerLayout::Cupertino) {
            let mut album_list: ViewRef<LinearLayout> = libview.find_name("album_sel").unwrap();
            let len = album_list.len();
            for idx in 0..len {
                let child = album_list.get_child_mut(idx).unwrap();
                if let Some(album_view) = child.downcast_mut::<LazyView<LinearLayout>>() {
                    if !album_view.is_initialized() {
                        album_view.initialize();
                        if idx > 0 { album_list.insert_child(idx, VDivider::new()); }
                        break;
                    }
                }
            }
        }

        status.set_cur_time(&cur_time);
        status.set_repeat(repeat);
        status.set_state(state);

        // keep the progress bar in sync with the playing audio
        player_prog.update(secs, song.duration, stopped);

        // hide the library view if the terminal width is too small
        libview.set_visible(player_prog.width >= 120);
    });

    root.add_global_callback('n', player_next);
    root.add_global_callback('p', player_prev);
    root.add_global_callback('s', player_stop);
    root.add_global_callback(' ', toggle_playback);
    root.add_global_callback('z', queue_shuffle);
    root.add_global_callback('r', cycle_repeat);
    root.add_global_callback('R', player_restart);

    // show the song finder
    root.add_global_callback(Event::CtrlChar('f'), |root| {
        let mut finder: ViewRef<HideableView<LinearLayout>> = root.find_name("finder").unwrap();
        finder.unhide();
        root.focus_name("finder").unwrap();
        finder.focus_view(&Selector::Name("song_term")).unwrap();
    });

    root.set_fps(30);
    root.add_global_callback('q', Cursive::quit);
    root.run();
}