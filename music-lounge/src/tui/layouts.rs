use super::{
    LibraryView,
    update_theme,
    views::{PlaybackProgress, PlayerStatusView},
    playlist_editor::add_to_playlist
};
use cursive_async_view::{AsyncView, AsyncState, AnimationFrame};
use cursive::{
    Cursive,
    view::{Nameable, Scrollable},
    event::{Event, Key, EventResult},
    align::HAlign,
    views::{
        LinearLayout,
        OnEventView,
        HideableView,
        ResizedView,
        TextView,
        ScrollView,
        NamedView,
        Button,
        ViewRef,
        EditView
    },
    traits::Resizable,
    utils::markup::StyledString
};
use cursive_extras::*;
use mlounge_client::MLoungeClient;
use mlounge_core::{
    PlayerLayout,
    library::{
        Artist,
        Album,
        Library,
        Playlist,
        PlaylistAddable,
        Song
    }
};
use std::{
    cell::Cell,
    rc::Rc,
    thread::{self, JoinHandle}
};

pub type PlayQueueView = OnEventView<NamedView<ScrollView<NamedView<AdvancedSelectView<Song>>>>>;
pub type PlayQueueScroller = ScrollView<NamedView<AdvancedSelectView<Song>>>;
pub type PlayQueueList = AdvancedSelectView<Song>;

static FRAMES: [&str; 4] = [
    "▄\n\
    ▄ ▄\n\
    ▄ ▄ ▄\n\n",
    "▄ ▄ ▄\n\
    ▄ ▄\n\
    ▄\n\n",
    "▄ ▄ ▄\n  \
       ▄ ▄\n    \
         ▄\n\n",
    "    ▄\n  \
       ▄ ▄\n\
     ▄ ▄ ▄\n\n"
];

thread_local! {
    static FRAME_COUNTER: Cell<usize> = Cell::new(0);
}

pub fn get_layout(layout: PlayerLayout, play_queue: PlayQueueView, root: &mut Cursive) -> LinearLayout {
    // play queue song finder search box
    let mut finder_input = styled_editview("", "song_term", false);
    finder_input
        .get_mut()
        .set_on_submit(|root, term| {
            let song_term = term.to_ascii_lowercase();
            let mut songs: ViewRef<PlayQueueList> = root.find_name("play_queue").unwrap();
            let selected = songs.selected_index();
            let mut new_sel = usize::MAX;

            for (i, (_, song)) in songs.iter().enumerate() {
                if i <= selected { continue; }
                else if song.name.to_ascii_lowercase().contains(&song_term) {
                    new_sel = i;
                    break;
                }
            }

            if new_sel != usize::MAX {
                root.focus_name("play_queue").unwrap();
                songs.set_selection(new_sel);
                drop(songs);
                let mut play_queue_scroller: ViewRef<PlayQueueScroller> = root.find_name("play_queue_scroll").unwrap();
                play_queue_scroller.set_offset((0, new_sel));
            }
        });

    // play queue song finder
    let song_finder =
        OnEventView::new(
            HideableView::new(
                hlayout!(
                    TextView::new("Find in play queue: ").fixed_width(20),
                    finder_input.full_width(),
                    fixed_hspacer(1),
                    Button::new("Previous", |root| find_queue_song(root, true)),
                    fixed_hspacer(1),
                    Button::new("Next", |root| find_queue_song(root, false))
                )
            )
                .hidden()
                .with_name("finder")
        )
            .on_event_inner(Event::Key(Key::Esc), |finder, _e| {
                finder.get_mut().hide();
                Some(EventResult::Ignored)
            });

    match layout {
        PlayerLayout::Metropolis => {
            vlayout!(
                VDivider::new(),
                hlayout!(
                    HideableView::new(get_lib_view(layout, root)).with_name("libview"),
                    HDivider::new(),
                    vlayout!(
                        hlayout!(
                            TextView::new("Artist").fixed_width(30).with_name("artist_l"),
                            TextView::new("Song").fixed_width(30).with_name("song_l"),
                            TextView::new("Album").fixed_width(30).with_name("album_l"),
                            TextView::new("Time")
                        ),
                        VDivider::new().with_name("pq_div").full_width(),
                        play_queue, song_finder
                    )
                ).with_name("player"),
                VDivider::new(),
                hlayout!(
                    ImageView::new(8, 4).minimize(true).with_name("album_art"),
                    PlayerStatusView::new().with_name("player_status")
                ),
                PlaybackProgress::new().with_name("progress")
            )
        }

        PlayerLayout::Classic => {
            vlayout!(
                VDivider::new(),
                hlayout!(
                    HideableView::new(get_lib_view(layout, root)).with_name("libview"),
                    HDivider::new(),
                    ResizedView::with_full_screen(
                        vlayout!(
                            hlayout!(
                                TextView::new("Artist").fixed_width(30).with_name("artist_l"),
                                TextView::new("Song").fixed_width(30).with_name("song_l"),
                                TextView::new("Album").fixed_width(30).with_name("album_l"),
                                TextView::new("Time")
                            ),
                            VDivider::new().with_name("pq_div").full_width(),
                            play_queue, song_finder
                        )
                    )
                )
                    .with_name("player"),
                VDivider::new(),
                hlayout!(
                    ImageView::new(6, 3).minimize(true).with_name("album_art"),
                    PlayerStatusView::new().with_name("player_status")
                ),
                PlaybackProgress::new().with_name("progress")
            )
        }

        PlayerLayout::Cupertino => {
            vlayout!(
                VDivider::new(),
                hlayout!(
                    filled_hspacer(),
                    ImageView::new(8, 4).minimize(true).with_name("album_art"),
                    PlayerStatusView::new().with_name("player_status"),
                    filled_hspacer()
                ),
                PlaybackProgress::new().with_name("progress"),
                hlayout!(
                    HideableView::new(get_lib_view(layout, root)).with_name("libview"),
                    HDivider::new(),
                    vlayout!(
                        hlayout!(
                            TextView::new("Artist").fixed_width(30).with_name("artist_l"),
                            TextView::new("Song").fixed_width(30).with_name("song_l"),
                            TextView::new("Album").fixed_width(30).with_name("album_l"),
                            TextView::new("Time")
                        ),
                        VDivider::new().with_name("pq_div").full_width(),
                        play_queue, song_finder
                    )
                )
                    .with_name("player")
            )
        }
    }
}

pub fn play_or_add_to_queue<P: PlaylistAddable>(view: &mut Cursive, items: &P) {
    let player = view.user_data::<MLoungeClient>().unwrap();
    if player.is_empty() {
        let mut temp_list = Playlist::new("temp");
        temp_list.add_items(items);
        player.load_list(&temp_list, &temp_list.songs[0]).unwrap();
    }
    else {
        player.queue.add_items(items);
        player.sync(false).unwrap();
    }
    update_theme(view, true);
}

fn build_album_view(artist: &Artist, album_sel: &mut LinearLayout) {
    use cursive::With;

    for (i, album) in artist.albums.iter().cloned().enumerate() {
        let view = LazyView::new(move || {
            let album_id = Rc::new(format!("{}{}", album.artist, album.name));
            let album_id_ref = album_id.clone();
            let id = Rc::new(i.to_string());
            let id_ref1 = id.clone();
            let id_ref2 = id.clone();

            let mut album_list = vlayout!(
                AdvancedButton::new_with_data(
                    StyledString::styled(format!("{} ({})", album.name, album.year), album.title_color()), album.clone(),
                    move |root| {
                        let button: ViewRef<AdvancedButton<Album>> = root.find_name(id_ref1.as_str()).unwrap();
                        play_or_add_album(root, button.get_data());
                    }
                )
                    .with_name(id.as_str())
                    .wrap_with(OnEventView::new)
                    .on_event('a', move |root| {
                        let button: ViewRef<AdvancedButton<Album>> = root.find_name(id_ref2.as_str()).unwrap();
                        add_to_playlist(root, button.get_data().clone())
                    })
                    .max_width(45),

                fixed_vspacer(1)
            );

            if !album.is_single() {
                let mut build_threads: Vec<Option<JoinHandle<(StyledString, Song)>>> = album.songs.iter()
                    .cloned()
                    .map(|song| {
                        Some(
                            thread::spawn(
                                move || (StyledString::styled(&song.name, song.list_colors().1), song)
                            )
                        )
                    })
                    .collect();

                album_list.add_child(
                    AdvancedSelectView::new()
                        .on_submit(play_or_add_song)
                        .with_all(
                            build_threads.iter_mut()
                                .filter_map(|thr|
                                    thr.take()
                                        .unwrap()
                                        .join()
                                        .ok()
                                )
                        )
                        .with_name(album_id.as_str())
                        .wrap_with(OnEventView::new)
                        .on_event('a', move |root| {
                            let album: ViewRef<AdvancedSelectView<Song>> = root.find_name(album_id_ref.as_str()).unwrap();
                            add_to_playlist(root, album.selection().unwrap().clone());
                        })
                        .max_width(45)
                );
            }

            let mut art = ImageView::new(12, 6);
            if let Some(image) = album.get_art_path() {
                art.set_image(&image);
            }

            hlayout!(
                art,
                fixed_hspacer(1),
                album_list
            )
        })
            .auto_init(false);

        album_sel.add_child(view);
    }
}

fn find_queue_song(root: &mut Cursive, reverse: bool) {
    let mut songs: ViewRef<PlayQueueList> =
        root.find_name("play_queue").expect("I swear this is the correct view!");
    let term: ViewRef<EditView> = root.find_name("song_term").unwrap();
    let song_term = term
        .get_content()
        .to_ascii_lowercase();

    let selected = songs.selected_index();
    let mut new_sel = selected;
    if reverse {
        for (i, (_, song)) in songs.iter().enumerate().rev() {
            if i >= selected { continue; }
            else if song.name.to_ascii_lowercase().contains(&song_term) {
                new_sel = i;
                break;
            }
        }
    }
    else {
        for (i, (_, song)) in songs.iter().enumerate() {
            if i <= selected { continue; }
            else if song.name.to_ascii_lowercase().contains(&song_term) {
                new_sel = i;
                break;
            }
        }
    }

    root.focus_name("play_queue").unwrap();
    songs.set_selection(new_sel);
    drop(songs);
    let mut play_queue_scroller: ViewRef<PlayQueueScroller> = root.find_name("play_queue_scroll").unwrap();
    play_queue_scroller.set_offset((0, new_sel));
}

fn get_lib_view(layout: PlayerLayout, root: &mut Cursive) -> LibraryView {
    AsyncView::new(root,
        move || {
            if let Ok(Some(lib)) = mlounge_client::get_library() {
                AsyncState::Available(build_lib_view(lib, layout).full_height())
            }
            else { AsyncState::Pending }
        }
    )
        .with_animation_fn(new_lib_loading_anim("Scanning Library..."))
}

pub fn new_lib_loading_anim(text: &'static str) -> impl (Fn(usize, usize, usize) -> AnimationFrame) {
    move |_w, _h, frame_idx| {
        let mut content = StyledString::new();

        FRAME_COUNTER.with(|frame_counter| {
            let cur_frame = frame_counter.get();
            let frame = FRAMES[cur_frame];
            content.append(format!("{frame}{text}"));

            if frame_idx % 30 == 0 {
                if cur_frame >= 3 {
                    frame_counter.set(0);
                }
                else {
                    frame_counter.set(cur_frame + 1);
                }
            }
        });

        AnimationFrame {
            content,
            next_frame_idx: (frame_idx + 1) % 60,
        }
    }
}

fn build_lib_view(library: Library, layout: PlayerLayout) -> LinearLayout {
    use cursive::With;
    match layout {
        PlayerLayout::Metropolis | PlayerLayout::Cupertino => {
            let artist_selector = AdvancedSelectView::new()
                .on_select(|root, artist| {
                    let mut album_sel: ViewRef<LinearLayout> = root.find_name("album_sel").unwrap();
                    *album_sel = vlayout!();
                    build_album_view(artist, &mut album_sel);
                })
                .on_submit(play_or_add_artist)
                .with_all(
                    library.iter()
                        .map(|artist|(&artist.name, artist.clone()))
                );

            let mut album_sel = vlayout!();
            if let Some(artist) = library.get_artist(0) {
                build_album_view(artist, &mut album_sel);
            }

            hlayout!(
                // artist selector
                vlayout!(
                    TextView::new("Artists").h_align(HAlign::Center),
                    VDivider::new(),
                    artist_selector
                        .with_name("artist_selector")
                        .wrap_with(OnEventView::new)
                        .on_event('a', |root| {
                            let selector: ViewRef<AdvancedSelectView<Artist>> = root.find_name("artist_selector").unwrap();
                            if let Some(selected) = selector.selection() { add_to_playlist(root, (*selected).clone()); }
                        })
                        .scrollable()
                )
                    .fixed_width(30),
                HDivider::new(),

                // album selector
                vlayout!(
                    TextView::new("Albums").h_align(HAlign::Center),
                    VDivider::new(),
                    album_sel.with_name("album_sel").scrollable()
                )
                    .fixed_width(60)
            )
        }

        PlayerLayout::Classic => {
            let artist_selector = AdvancedSelectView::new()
                .on_select(|root, artist| {
                    fill_artist_albums(root, artist);
                    fill_album_songs(root, &artist.albums[0]);
                })
                .on_submit(play_or_add_artist)
                .with_all(
                    library.iter()
                        .map(|artist| (&artist.name, artist.clone()))
                );

            let mut album_selector = AdvancedSelectView::new()
                .on_select(fill_album_songs)
                .on_submit(play_or_add_album);

            let mut song_selector = AdvancedSelectView::new()
                .on_submit(play_or_add_song);

            if let Some(artist) = library.get_artist(0) {
                for album in &artist.albums {
                    let name = format!("({}) {}", album.year, album.name);
                    album_selector.add_item(name, album.clone());
                }

                if let Some(album) = artist.albums.get(0) {
                    song_selector.add_all(
                        album.songs.iter()
                            .map(|song| (&song.name, song.clone()))
                    );
                }
            }

            hlayout!(
                // artist selector
                vlayout!(
                    TextView::new("Artists").h_align(HAlign::Center),
                    VDivider::new(),
                    artist_selector
                        .with_name("artist_selector")
                        .wrap_with(OnEventView::new)
                        .on_event('a', |root| {
                            let selector: ViewRef<AdvancedSelectView<Artist>> = root.find_name("artist_selector").unwrap();
                            if let Some(selected) = selector.selection() {
                                add_to_playlist(root, (*selected).clone());
                            }
                        })
                        .scrollable()
                )
                    .fixed_width(30),

                HDivider::new(),

                // album selector
                vlayout!(
                    TextView::new("Albums").h_align(HAlign::Center),
                    VDivider::new(),
                    album_selector
                        .with_name("album_selector")
                        .wrap_with(OnEventView::new)
                        .on_event('a', |root| {
                            let selector: ViewRef<AdvancedSelectView<Album>> = root.find_name("album_selector").unwrap();
                            if let Some(selected) = selector.selection() { add_to_playlist(root, (*selected).clone()); }
                        })
                        .scrollable()
                )
                    .fixed_width(30),

                HDivider::new(),

                // song selector
                vlayout!(
                    TextView::new("Songs").h_align(HAlign::Center),
                    VDivider::new(),
                    song_selector
                        .with_name("song_selector")
                        .wrap_with(OnEventView::new)
                        .on_event('a', |root| {
                            let selector: ViewRef<AdvancedSelectView<Song>> = root.find_name("song_selector").unwrap();
                            if let Some(selected) = selector.selection() { add_to_playlist(root, (*selected).clone()); }
                        })
                        .scrollable()
                )
                    .fixed_width(30)
            )
        }
    }
}

fn fill_artist_albums(root: &mut Cursive, artist: &Artist) {
    let mut selector: ViewRef<AdvancedSelectView<Album>> = root.find_name("album_selector").unwrap();
    selector.clear();
    for album in &artist.albums {
        let name = format!("({}) {}", album.year, album.name);
        selector.add_item(name, album.clone());
    }
}

fn fill_album_songs(root: &mut Cursive, album: &Album) {
    let mut selector: ViewRef<AdvancedSelectView<Song>> = root.find_name("song_selector").unwrap();
    selector.clear();
    for song in &album.songs {
        selector.add_item(&song.name, song.clone());
    }
}

fn play_or_add_artist(view: &mut Cursive, artist: &Artist) { play_or_add_to_queue(view, artist); }
fn play_or_add_album(view: &mut Cursive, album: &Album) { play_or_add_to_queue(view, album); }
fn play_or_add_song(view: &mut Cursive, song: &Song) { play_or_add_to_queue(view, song); }