use crate::{
    CONFIG,
    tui::PlayerLayout
};
use cursive::{
    Printer, Vec2,
    traits::View,
    direction::Direction,
    view::CannotFocus,
    utils::markup::StyledString,
    event::{Event, EventResult, Key, MouseButton, MouseEvent},
    theme::{
        Color,
        ColorStyle,
        BaseColor,
        PaletteColor
    }
};
use cursive_extras::SpannedStrExt;
use unicode_width::UnicodeWidthStr;
use mlounge_client::{MLoungeClient, PlayerState, RepeatMode};
use mlounge_core::{
    config::{ThemeConfig, Config},
    library::Song
};

// custom view for a playback progress bar
pub struct PlaybackProgress {
    pub width: usize,
    cur_time: u64,
    duration: u64,
    color: Color,
    selected: bool,
    stopped: bool
}

impl PlaybackProgress {
    pub fn new() -> PlaybackProgress {
        PlaybackProgress {
            width: 0,
            cur_time: 0,
            duration: 0,
            color: Color::Light(BaseColor::Green),
            selected: false,
            stopped: true
        }
    }

    pub fn update(&mut self, length: u64, total: u64, stopped: bool) {
        self.stopped = stopped;
        self.duration = total;
        self.cur_time = length;
    }

    pub fn set_color(&mut self, color: Color) { self.color = color; }
}

impl View for PlaybackProgress {
    fn draw(&self, printer: &Printer) {
        if matches!(CONFIG.layout, PlayerLayout::Classic | PlayerLayout::Metropolis) && self.duration == 0 {
            return;
        }

        let style = if self.selected && printer.focused {
            printer.with_color(ColorStyle::new(Color::TerminalDefault, printer.theme.palette[PaletteColor::Primary]), |p| p.print_hline((0, 0), self.width, "─"));
            ColorStyle::new(Color::TerminalDefault, self.color)
        }
        else {
            printer.print_hline((0, 0), self.width, "─");
            ColorStyle::new(self.color, Color::TerminalDefault)
        };

        let size = printer.size;
        let to_fill_f = size.x as f32 * (self.cur_time as f32 / self.duration as f32);
        let to_fill = to_fill_f as usize;

        printer.with_color(style, |p| {
            p.print_hline((0, 0), to_fill, "=");
            if to_fill > 0 {
                p.print((to_fill - 1, 0), ">");
            }
        });
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x;
        Vec2::new(self.width, (!matches!(CONFIG.layout, PlayerLayout::Classic | PlayerLayout::Metropolis) || self.duration > 0) as usize)
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        match event {
            Event::FocusLost => {
                self.selected = false;
                EventResult::consumed()
            }

            Event::Key(Key::Left) => {
                EventResult::with_cb(|view| {
                    let player = view.user_data::<MLoungeClient>().unwrap();
                    let time = player.cur_time_secs();
                    if time > 0 {
                        player.seek(time - 1);
                    }
                    player.quick_sync();
                })
            }

            Event::Key(Key::Right) => {
                EventResult::with_cb(|view| {
                    let player = view.user_data::<MLoungeClient>().unwrap();
                    let time = player.cur_time_secs();
                    player.seek(time + 1);
                    player.quick_sync();
                })
            }

            Event::Mouse { event: MouseEvent::Press(MouseButton::Left), position, .. } => {
                let pos = position.x;
                let location = pos as f32 / self.width as f32;
                let seek_time = (location * self.duration as f32) as u64;
                EventResult::with_cb_once(move |view| {
                    let player = view.user_data::<MLoungeClient>().unwrap();
                    player.seek(seek_time);
                    player.quick_sync();
                })
            }

            _ => EventResult::Ignored
        }
    }

    fn take_focus(&mut self, _: Direction) -> Result<EventResult, CannotFocus> {
        if self.stopped {
            self.selected = false;
            return Err(CannotFocus);
        }
        self.selected = true;
        Ok(EventResult::consumed())
    }
}

pub struct PlayerStatusView {
    layout: PlayerLayout,
    song: Song,
    repeat: RepeatMode,
    state: PlayerState,
    cur_time: String,
    duration: String,
    theme: [Color; 3]
}

impl PlayerStatusView {
    pub fn new() -> PlayerStatusView {
        let theme_cfg = ThemeConfig::load().get_custom_theme();
        PlayerStatusView {
            layout: CONFIG.layout,
            song: Song::empty(),
            repeat: RepeatMode::Once,
            state: PlayerState::Stopped,
            cur_time: "0:00".to_string(),
            duration: "0:00".to_string(),
            theme: [
                theme_cfg["np_song"],
                theme_cfg["np_album"],
                theme_cfg["np_artist"]
            ]
        }
    }

    pub fn set_song(&mut self, song: Song) {
        self.duration = song.get_duration_str();
        self.song = song;
        self.cur_time = "0:00".to_string();
    }

    pub fn set_repeat(&mut self, repeat: RepeatMode) { self.repeat = repeat; }
    pub fn set_state(&mut self, state: PlayerState) { self.state = state; }
    pub fn set_cur_time(&mut self, cur_time: &str) { self.cur_time = cur_time.to_string(); }
    pub fn set_colors(&mut self, colors: [Color; 3]) { self.theme = colors; }
}

impl View for PlayerStatusView {
    fn draw(&self, printer: &Printer) {
        if matches!(self.state, PlayerState::Stopped) {
            printer.print((0, 0), "[stopped]");
            return;
        }

        let offset = if CONFIG.show_album_thumb { 1 } else { 0 };
        let [song_color, album_color, artist_color] = self.theme;
        let mut artist_album_line = StyledString::styled(&self.song.artist, artist_color);
        artist_album_line.append(" - ");
        artist_album_line.append_styled(&self.song.album, album_color);
        let state_time_str = format!("{} / {} {}", self.cur_time, self.duration, self.state);
        printer.with_color(ColorStyle::from(song_color), |printer| printer.print((offset, 0), &self.song.name));
        printer.print_styled((offset, 1), artist_album_line.as_spanned_str());
        printer.print((offset, 2), &state_time_str);
        let repeat = format!("Repeat: {}", self.repeat);
        if matches!(self.layout, PlayerLayout::Cupertino | PlayerLayout::Metropolis) {
            printer.print((offset, 3), &repeat);
        }
        else {
            let repeat_len = self.repeat.disp_width() + 8;
            printer.print((printer.size.x - repeat_len, 2), &repeat);
        }
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        if matches!(self.state, PlayerState::Stopped) {
            return (9, 1).into();
        }
        match self.layout {
            PlayerLayout::Classic => (constraint.x, 3).into(),

            PlayerLayout::Metropolis | PlayerLayout::Cupertino => {
                let line_lens = [
                    self.song.name.width() + 1, // width of song title
                    self.song.artist.width() + self.song.album.width() + 4, // width of artist and album names with spacer

                    // width of duration and current time with spacer
                    // and width of the displayed player state
                    self.cur_time.width() + self.duration.width() + self.state.disp_width() + 5,
                    self.repeat.disp_width() + 14
                ];

                let mut greatest = line_lens[0];
                for width in line_lens {
                    if width > greatest { greatest = width }
                }
                (greatest, 4).into()
            }
        }
    }
}