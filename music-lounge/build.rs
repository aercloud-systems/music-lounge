use std::{fs, env};
use rust_utils::linux::{
    DesktopEntryFileBuilder,
    ManpageBuilder,
    AurPkgbuildBuilder
};

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    fs::create_dir_all("../man/").unwrap_or_default();

    ManpageBuilder::new(
        "mlounge",
        "Music Lounge player interface",
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "This is the player interface of Music Lounge. If the background process is not already started, the interface will start it.",
    )
        .name_comment("part of Music Lounge")
        .other_page("mlounge-visual")
        .other_page("mlounged")
        .build(VERSION, "../man/mlounge.1");

    ManpageBuilder::new(
        "mlounged",
        "Music Lounge player process",
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "This is the player process of Music Lounge.",
    )
        .name_comment("part of Music Lounge")
        .other_page("mlounge-visual")
        .other_page("mlounge")
        .section(
            "EXAMPLES",
            "Start manually: mlounged &\n\
            Start with systemd: systemctl start --user mlounged"
        )
        .build(VERSION, "../man/mlounged.1");

    ManpageBuilder::new(
        "mlounge-visual",
        "Music Lounge audio visualizer",
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "The visualizers:\n\
        wave: traditional spectrum wave visualizer\n\
        matrix: matrix digital rain that flickers at the beat of the music\n\
        info: displays the info of the current song"
    )
        .name_comment("part of Music Lounge")
        .other_page("mlounged")
        .other_page("mlounge")
        .section("OPTIONS", "Valid visualizer names are wave, matrix, and info")
        .section("SYNOPSIS", "mlounge-visual <name>")
        .section(
            "EXAMPLES",
            "mlounge-visual wave\n\
            mlounge-visual matrix\n\
            mlounge-visual info"
        )
        .build(VERSION, "../man/mlounge-visual.1"); 

    DesktopEntryFileBuilder::new("Music Lounge", "mlounge", true, VERSION)
        .comment("Yet another music player for Unix-like systems")
        .icon("/usr/share/mlounge/icon.png")
        .category("AudioVideo")
        .category("Player")
        .build("../mlounge.desktop");

    AurPkgbuildBuilder::new(
        "music-lounge",
        VERSION,
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "Yet another music player",
        "https://gitlab.com/aercloud-systems/music-lounge/-/archive/$pkgver/music-lounge-$pkgver.zip",
        "https://gitlab.com/aercloud-systems/music-lounge",
        "GPL3",
        "cd \"music-lounge-$pkgver\"\n\
        cargo build --release\n\
        cd target/release\n\
        ln -sf mlounge-visual mlounge-matrix\n\
        ln -sf mlounge-visual mlounge-wave\n\
        ln -sf mlounge-visual mlounge-info",
        "cd \"music-lounge-$pkgver\"\n\
        mkdir -p \"$pkgdir/usr/share/mlounge/font\"\n\
        install -Dt \"$pkgdir/usr/share/man/man1\" man/mlounge.1\n\
        install -Dt \"$pkgdir/usr/share/man/man1\" man/mlounged.1\n\
        install -Dt \"$pkgdir/usr/share/man/man1\" man/mlounge-visual.1\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/mlounge\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/mlounged\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/mlounge-visual\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/mlounge-info\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/mlounge-matrix\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/mlounge-wave\n\
        install -Dt \"$pkgdir/usr/share/applications/\" mlounge.desktop\n\
        install -Dt \"$pkgdir/usr/share/mlounge/\" icon.png\n\
        install -Dt \"$pkgdir/usr/share/mlounge/\" matrix.kwinrule\n\
        install -Dt \"$pkgdir/usr/share/mlounge/\" wave.kwinrule\n\
        install -Dt \"$pkgdir/usr/share/mlounge/\" info.kwinrule\n\
        install -Dt \"$pkgdir/usr/lib/systemd/user\" mlounged.service\n\
        install -Dt \"$pkgdir/usr/share/mlounge/font\" font/Monocraft.otf\n\
        install -Dt \"$pkgdir/usr/share/mlounge/font\" \"font/Monocraft License.txt\""
    )
        .dependency("alsa-lib")
        .dependency("gcc-libs")
        .dependency("glibc")
        .dependency("dbus")
        .dependency("systemd-libs")
        .dependency("libcap")
        .dependency("libgcrypt")
        .dependency("lz4")
        .dependency("xz")
        .dependency("zstd")
        .dependency("libgpg-error")
        .make_dependency("cargo")
        .make_dependency("gzip")
        .build("../aur");
}