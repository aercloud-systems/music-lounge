#!/bin/sh
cargo build --release
sudo mkdir /usr/share/mlounge
sudo cp icon.png /usr/share/mlounge/icon.png
sudo cp target/release/mlounge /usr/bin/mlounge
sudo chmod 755 /usr/bin/mlounge
sudo cp mlounge.desktop /usr/share/applications/mlounge.desktop
sudo ln -sf /usr/bin/mlounge /usr/bin/mlounged

echo "Music Lounge has been installed on your system"
